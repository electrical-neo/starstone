# Starstone
Starstone is an x86_64 OS written in Zig

# Dependencies:
- `xorriso`
- `mtools`
- `zig` (version 0.11)

# Quick start
```bash
cd nebula && zig build --prefix ../util && cd ..
cd starcore && zig build --prefix ../iso && cd ..
./util/bin/nebula-mkiso iso -o starstone.iso
qemu-system-x86_64 -cdrom starstone.iso # or any other VM
```

# Project structure
- `helios` - common code for `starcore` and `nebula`
- `starcore` - the kernel
- `nebula` - the bootloader
