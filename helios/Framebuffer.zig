base: u64,
width: u32,
height: u32,
pitch: u32,
pixel_size: u8,

pub fn clear(self: @This(), r: u8, g: u8, b: u8) void {
    for (0..self.height) |y| {
        for (0..self.width) |x| {
            self.putPixel(@intCast(x), @intCast(y), r, g, b);
        }
    }
}

pub inline fn putPixel(self: @This(), x: u32, y: u32, r: u8, b: u8, g: u8) void {
    @as(*u8, @ptrFromInt(self.base + x * self.pixel_size + y * self.pitch + 0)).* = r;
    @as(*u8, @ptrFromInt(self.base + x * self.pixel_size + y * self.pitch + 1)).* = g;
    @as(*u8, @ptrFromInt(self.base + x * self.pixel_size + y * self.pitch + 2)).* = b;
}

pub fn rectangle(self: @This(), x: u32, y: u32, width: u32, height: u32) !@This() {
    if (x + width > self.width or y + height > self.height)
        return error.InvalidCoord;

    return .{
        .base = self.base + self.pitch * y + self.pixel_size * x,
        .width = width,
        .height = height,
        .pitch = self.pitch,
        .pixel_size = self.pixel_size,
    };
}
