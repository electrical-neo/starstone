const std = @import("std");
const boot = @import("boot.zig");

pub const MemoryChunk = struct {
    pub const Type = enum(u8) {
        // only exists to be replaced with reserved when the memory map is populated
        hole = 0,
        // free memory
        free,
        // free memory under 1MB
        low_memory,
        // used memory
        used,
        // acpi reclaimable
        acpi_reclaimable,
        // anything else
        reserved,
        // only exists because i had troubles merging chunks backwards
        head,
    };

    type: Type,
    len: u64,
    next: ?*MemoryChunk = null,
    prev: ?*MemoryChunk = null,

    fn split(self: *@This(), allocator: std.mem.Allocator, len: u64) !*@This() {
        const new = try allocator.create(@This());
        new.* = .{
            .type = self.type,
            .len = self.len - len,
            .next = self.next,
            .prev = self,
        };

        if (self.next) |n| {
            n.prev = new;
        }

        self.next = new;
        self.len = len;

        return new;
    }

    fn setType(self: *@This(), allocator: std.mem.Allocator, typ: Type) void {
        self.type = typ;
        if (self.next) |next| {
            if (next.type == typ) {
                self.len += next.len;
                self.next = next.next;
                if (next.next) |next_next|
                    next_next.prev = self;
                allocator.destroy(next);
            }
        }

        if (self.prev) |prev| {
            if (prev.type == typ) {
                self.len += prev.len;
                self.prev = prev.prev;
                if (prev.prev) |prev_prev|
                    prev_prev.next = self;
                allocator.destroy(prev);
            }
        }
    }
};

allocator: std.mem.Allocator,
head: *MemoryChunk,

pub fn init(allocator: std.mem.Allocator, len: u64) !@This() {
    const head = try allocator.create(MemoryChunk);
    head.* = .{
        .type = .head,
        .len = 0,
    };

    const first = try allocator.create(MemoryChunk);
    first.* = .{
        .type = .hole,
        .len = len,
        .prev = head,
    };

    head.next = first;

    return .{
        .allocator = allocator,
        .head = head,
    };
}

pub fn restrictArea(self: *@This(), base: u64, len: u64, typ: MemoryChunk.Type) !void {
    var chunk_base: u64 = 0;
    var chunk: ?*MemoryChunk = self.head.next;
    while (chunk) |c| {
        const chunk_len = c.len;
        defer chunk_base += chunk_len;

        if (chunk_base > base + len)
            return;

        if (@intFromEnum(c.type) >= @intFromEnum(typ)) {
            chunk = c.next;
            continue;
        }

        if (base > chunk_base and base < chunk_base + c.len and base + len > chunk_base + c.len) {
            // end overlaps
            const new = try c.split(self.allocator, base - chunk_base);
            new.setType(self.allocator, typ);
            chunk = new.next;
        } else if (base <= chunk_base and base + len > chunk_base and base + len < chunk_base + c.len) {
            // beginning overlaps
            _ = try c.split(self.allocator, base + len - chunk_base);
            c.setType(self.allocator, typ);
            return;
        } else if (base > chunk_base and base + len < chunk_base + c.len) {
            // middle overlaps
            const middle = try c.split(self.allocator, base - chunk_base);
            _ = try middle.split(self.allocator, len);
            middle.setType(self.allocator, typ);
            return;
        } else if (base <= chunk_base and base + len >= chunk_base + c.len) {
            // whole chunk overlaps
            c.setType(self.allocator, typ);
            chunk = c.next;
        } else {
            chunk = c.next;
        }
    }
}

pub fn reserveHoles(self: *@This()) void {
    var chunk: ?*MemoryChunk = self.head.next;
    while (chunk) |c| {
        defer chunk = c.next;

        if (c.type == .hole)
            c.setType(self.allocator, .reserved);
    }
}

pub fn reserveUsed(self: *@This()) void {
    var chunk: ?*MemoryChunk = self.head.next;
    while (chunk) |c| {
        defer chunk = c.next;

        if (c.type == .used)
            c.setType(self.allocator, .reserved);
    }
}

pub fn requestPageFrame(self: *@This()) !u64 {
    var chunk: ?*MemoryChunk = self.head.next;
    var chunk_base: u64 = 0;
    while (chunk) |c| {
        defer chunk = c.next;
        defer chunk_base += c.len;

        if (c.type == .free) {
            const page = (chunk_base + 0xfff) & ~@as(u64, 0xfff);
            if (page + 0x1000 <= chunk_base + c.len) {
                try self.restrictArea(page, 0x1000, .reserved);
                return page;
            }
        }
    }

    return error.OutOfMemory;
}

pub fn getChunkCount(self: *const @This()) u64 {
    var chunk: ?*MemoryChunk = self.head.next;
    var i: usize = 0;
    while (chunk) |c| {
        defer chunk = c.next;
        defer i += 1;
    }
    return i;
}

pub fn intoNebulaBootMmap(self: *const @This(), out: []boot.MemoryMapEntry) !u64 {
    var chunk: ?*MemoryChunk = self.head.next;
    var i: usize = 0;
    while (chunk) |c| {
        defer chunk = c.next;
        defer i += 1;

        if (out.len <= i)
            return error.BufferTooSmall;

        out[i] = .{
            .type = switch (c.type) {
                .free => .free,
                .low_memory => .low_memory,
                .acpi_reclaimable => .acpi_reclaimable,
                .reserved => .reserved,
                else => return error.InvalidMemoryMap,
            },
            .length = @intCast(c.len),
        };
    }

    return i;
}
