const std = @import("std");
const Framebuffer = @import("Framebuffer.zig");

pub const Font = [256][16]u8;
pub const Writer = std.io.Writer(*@This(), error{}, write);

fb: Framebuffer,
font: *Font,

x: u32 = 0,
y: u32 = 0,

pub fn clear(self: *@This()) void {
    self.fb.clear(0x23, 0x26, 0x27);
    self.x = 0;
    self.y = 0;
}

fn scroll(self: *@This()) void {
    // TODO: double buffering
    for (16..self.fb.height) |y| {
        @memcpy(@as([*]u8, @ptrFromInt(self.fb.base + (y - 16) * self.fb.pitch))[0 .. self.fb.width * self.fb.pixel_size], @as([*]u8, @ptrFromInt(self.fb.base + y * self.fb.pitch))[0 .. self.fb.width * self.fb.pixel_size]);
    }

    for (self.fb.height - 16..self.fb.height) |y| {
        for (0..self.fb.width) |x| {
            self.fb.putPixel(@intCast(x), @intCast(y), 0x23, 0x26, 0x27);
        }
    }

    self.y -= 16;
}

fn lineFeed(self: *@This()) void {
    self.x = 0;
    self.y += 16;

    if (self.y + 16 > self.fb.height)
        self.scroll();
}

pub fn putc(self: *@This(), c: u8) void {
    if (c == '\n') {
        self.lineFeed();
        return;
    }

    if (self.x + 8 > self.fb.width)
        self.lineFeed();

    for (0..16) |y| {
        for (0..8) |x| {
            if (self.font[c][y] & (@as(u8, 1) << @intCast(7 - x)) != 0) {
                self.fb.putPixel(@intCast(self.x + x), @intCast(self.y + y), 0xfc, 0xfc, 0xfc);
            }
        }
    }

    self.x += 9;
}

fn write(self: *@This(), bytes: []const u8) error{}!usize {
    for (bytes) |byte| {
        self.putc(byte);
    }
    return bytes.len;
}

pub fn writer(self: *@This()) Writer {
    return .{ .context = self };
}
