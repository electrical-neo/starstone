pub const SdpType = enum { rsdp, xsdp };

pub const Sdp = union(SdpType) {
    rsdp: *align(1) const Rsdp,
    xsdp: *align(1) const Xsdp,
};

pub const Rsdp = extern struct {
    const signature = 0x2052545020445352;

    signature: u64 align(1),
    checksum: u8 align(1),
    oemid: [6]u8 align(1),
    revision: u8 align(1),
    rsdt_addr: u32 align(1),

    pub fn validate(self: *align(1) const @This()) bool {
        if (self.signature != signature)
            return false;

        var checksum: u8 = 0;
        for (0..@sizeOf(@This())) |i| {
            checksum +%= @as(*align(1) const u8, @ptrFromInt(@intFromPtr(self) + i)).*;
        }

        return checksum == 0;
    }
};

pub const Xsdp = extern struct {
    rsdp: Rsdp align(1),
    length: u32 align(1),
    xdst_addr: u64 align(1),
    ext_checksum: u8 align(1),
    reserved: [3]u8 align(1),

    pub fn validate(self: *align(1) const @This()) bool {
        if (!self.rsdp.validate())
            return false;

        var checksum: u8 = 0;
        for (0..@sizeOf(@This())) |i| {
            checksum +%= @as(*align(1) const u8, @ptrFromInt(@intFromPtr(self) + i)).*;
        }

        return checksum == 0;
    }
};
