const std = @import("std");
const Framebuffer = @import("Framebuffer.zig");
const Terminal = @import("Terminal.zig");

pub const DwarfSection = enum(u8) {
    debug_info = 0,
    debug_abbrev,
    debug_str,
    debug_str_offsets,
    debug_line,
    debug_line_str,
    debug_ranges,
    debug_loclists,
    debug_rnglists,
    debug_addr,
    debug_names,
    debug_frame,
    eh_frame,
    eh_frame_hdr,
};

pub const dwarf_section_count = std.enums.directEnumArrayLen(DwarfSection, 0);

pub const MemoryMapEntry = packed struct {
    pub const Type = enum(u2) {
        free,
        low_memory,
        acpi_reclaimable,
        reserved,
    };

    type: Type,
    length: u62,
};

pub const NebulaBootInfo = extern struct {
    framebuffer: extern struct {
        base: u64,
        width: u32,
        height: u32,
        pitch: u32,
        pixel_size: u8,
    },

    dwarf: [dwarf_section_count]extern struct {
        base: u64 = 0,
        size: u64 = 0,
    },

    rsdt: u32,
    xsdt: u64,
    mmap_entries: u64,

    comptime {
        std.debug.assert(@sizeOf(@This()) < 4096);
    }
};

pub const boot_info: *NebulaBootInfo = @ptrFromInt(0xffffff7fc0000000);
pub const font: *Terminal.Font = @ptrFromInt(0xffffff7fc0001000);
pub const mmap: *[512]MemoryMapEntry = @ptrFromInt(0xffffff7fc0002000);
pub const boot_info_var_data = 0xffffff7fc0003000;
pub const framebuffer_page_start = 0xffffff7fe0000000;
