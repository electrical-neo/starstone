const std = @import("std");

// TODO: SUSP
// TODO: Rock Ridge
// TODO: interleaving

fn LsbMsb(comptime int: type) type {
    return extern struct {
        lsb: int align(1),
        msb: int align(1),
    };
}

const VolumeDescriptor = extern struct {
    const Type = enum(u8) {
        primary = 1,
        terminator = 255,
        _,
    };

    const Primary = extern struct {
        unused0: u8 align(1),
        sys_id: [32]u8 align(1),
        vol_id: [32]u8 align(1),
        unused1: [8]u8 align(1),
        vol_space_size: LsbMsb(u32) align(1),
        unused2: [32]u8 align(1),
        vol_set_size: LsbMsb(u16) align(1),
        vol_seq_num: LsbMsb(u16) align(1),
        block_size: LsbMsb(u16) align(1),
        path_table_size: LsbMsb(u32) align(1),
        lpath_table_loc: u32 align(1),
        opt_lpath_table_loc: u32 align(1),
        mpath_table_loc: u32 align(1),
        opt_mpath_table_loc: u32 align(1),
        root_dir: [34]u8 align(1),
        vol_set_id: [128]u8 align(1),
        pub_id: [128]u8 align(1),
        data_prep_id: [128]u8 align(1),
        app_id: [128]u8 align(1),
        copyright_file_id: [37]u8 align(1),
        abstract_file_id: [37]u8 align(1),
        bibliographic_file_id: [37]u8 align(1),
        ctime: [17]u8 align(1),
        mtime: [17]u8 align(1),
        extime: [17]u8 align(1),
        eftime: [17]u8 align(1),
        file_scruct_ver: u8 align(1),
        unused3: u8 align(1),
        app_used: [512]u8 align(1),
        reserved: [653]u8 align(1),
    };

    type: Type align(1),
    identifier: [5]u8 align(1),
    version: u8 align(1),
    data: [2041]u8 align(1),

    const Data = union(enum) {
        primary: *const Primary,
        terminator: void,
        other: void,

        fn init(vol_desc: *const VolumeDescriptor) @This() {
            return switch (vol_desc.type) {
                .primary => .{ .primary = @ptrCast(&vol_desc.data) },
                .terminator => .terminator,
                _ => .other,
            };
        }
    };

    fn verify(self: *const @This()) !void {
        if (!std.mem.eql(u8, &self.identifier, "CD001"))
            return error.InvalidMagic;
    }

    fn getData(self: *const @This()) Data {
        return Data.init(self);
    }
};

const DirectoryRecord = struct {
    const Header = extern struct {
        len: u8 align(1),
        ext_attr_rec_len: u8 align(1),
        lba: LsbMsb(u32) align(1),
        size: LsbMsb(u32) align(1),
        rtime: [7]u8 align(1),
        flags: u8 align(1),
        interleave_unit_size: u8 align(1),
        interleave_gap_size: u8 align(1),
        vol_seq_num: LsbMsb(u16) align(1),
        file_name_len: u8 align(1),
    };

    header: Header,
    file_name: []u8,
    allocator: std.mem.Allocator,

    fn init(allocator: std.mem.Allocator, parent_dir: anytype) !@This() {
        var buf: [256]u8 = undefined;

        const reader = parent_dir.reader();
        const seekable_stream = parent_dir.seekableStream();

        while (blk: {
            buf[0] = try reader.readByte();
            break :blk buf[0] == 0;
        }) {
            try seekable_stream.seekTo(((try seekable_stream.getPos() + parent_dir.mount.block_size - 1) / parent_dir.mount.block_size) * parent_dir.mount.block_size);
        }

        try reader.readNoEof(buf[1..buf[0]]);
        const header = std.mem.bytesToValue(Header, buf[0..@sizeOf(Header)]);
        const file_name = try allocator.alloc(u8, header.file_name_len);
        @memcpy(file_name, buf[@sizeOf(Header) .. @sizeOf(Header) + file_name.len]);
        return .{
            .header = header,
            .file_name = file_name,
            .allocator = allocator,
        };
    }

    fn checkName(self: *const @This(), file_name: []const u8) bool {
        if (std.mem.eql(u8, self.file_name, &[_]u8{0}) and std.mem.eql(u8, file_name, "."))
            return true;
        if (std.mem.eql(u8, self.file_name, &[_]u8{1}) and std.mem.eql(u8, file_name, ".."))
            return true;

        return std.mem.eql(u8, self.file_name, file_name);
    }

    fn deinit(self: *const @This()) void {
        self.allocator.free(self.file_name);
    }
};

fn File(comptime MountFs: type) type {
    return struct {
        const Reader = std.io.Reader(*@This(), anyerror, read);
        const SeekableStream = std.io.SeekableStream(*@This(), anyerror, error{}, seekTo, seekBy, getPos, getEndPos);

        const Type = enum { file, directory };

        type: Type,
        lba: u32,
        size: u32,
        mount: *const MountFs,
        seek: u64 = 0,

        fn init(mount_fs: anytype, header: DirectoryRecord.Header) !@This() {
            if (header.interleave_unit_size != 0)
                return error.InterleavingUnsupported;

            return .{
                .type = if (header.flags & 2 == 0) .file else .directory,
                .lba = header.lba.lsb,
                .size = header.size.lsb,
                .mount = mount_fs,
            };
        }

        pub fn reader(self: *@This()) Reader {
            return .{ .context = self };
        }

        pub fn seekableStream(self: *@This()) SeekableStream {
            return .{ .context = self };
        }

        fn read(self: *@This(), buf: []u8) anyerror!usize {
            try self.mount.seekable_stream.seekTo(self.lba * self.mount.block_size + self.seek);
            const read_size = @min(buf.len, self.size -| self.seek);
            const actual_read_size = try self.mount.reader.read(buf[0..read_size]);
            self.seek += actual_read_size;
            return actual_read_size;
        }

        fn seekTo(self: *@This(), pos: u64) !void {
            self.seek = pos;
        }

        fn seekBy(self: *@This(), amt: i64) anyerror!void {
            if (amt < 0) {
                if (self.seek < -amt)
                    return error.Unseekable;
                self.seek -= @as(u64, @intCast(-amt));
            } else {
                if (self.seek +% @as(u64, @intCast(amt)) != self.seek +| @as(u64, @intCast(amt)))
                    return error.Unseekable;
                self.seek += @as(u64, @intCast(amt));
            }
        }

        fn getPos(self: *@This()) !u64 {
            return self.seek;
        }

        fn getEndPos(self: *@This()) !u64 {
            return self.size;
        }

        pub fn listDirectory(self: *@This(), out: anytype) !void {
            if (self.type != .directory)
                return error.NotADirectory;
            self.seek = 0;

            var buf: [512]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(&buf);
            const allocator = fba.allocator();

            var total: u32 = 0;

            while (DirectoryRecord.init(allocator, self)) |rec| {
                total += 1;
                if (rec.header.flags & 2 == 0) {
                    try out.print("    ", .{});
                } else {
                    try out.print("DIR ", .{});
                }

                if (std.mem.eql(u8, rec.file_name, &[_]u8{0})) {
                    try out.print(".\n", .{});
                } else if (std.mem.eql(u8, rec.file_name, &[_]u8{1})) {
                    try out.print("..\n", .{});
                } else {
                    try out.print("{s}\n", .{rec.file_name});
                }
                rec.deinit();
            } else |err| {
                if (err != error.EndOfStream)
                    return err;
            }

            try out.print("TOTAL {}\n", .{total});
        }

        pub fn openFile(self: *@This(), file_name: []const u8) !File(MountFs) {
            if (self.type != .directory)
                return error.NotADirectory;
            self.seek = 0;

            var buf: [512]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(&buf);
            const allocator = fba.allocator();

            while (DirectoryRecord.init(allocator, self)) |rec| {
                if (rec.checkName(file_name))
                    return File(MountFs).init(self.mount, rec.header);
                rec.deinit();
            } else |err| {
                if (err != error.EndOfStream)
                    return err;
                return error.FileNotFound;
            }
        }
    };
}

pub fn Mount(comptime Reader: type, comptime SeekableStream: type) type {
    return struct {
        reader: Reader,
        seekable_stream: SeekableStream,
        root_dir: DirectoryRecord.Header,
        block_size: u32,

        fn init(reader: Reader, seekable_stream: SeekableStream) !@This() {
            try seekable_stream.seekTo(32 * 1024);

            const pvd = while (true) {
                const vol_desc = try reader.readStruct(VolumeDescriptor);
                try vol_desc.verify();

                switch (vol_desc.getData()) {
                    .primary => |pvd| break pvd,
                    .terminator => return error.NoPvd,
                    .other => {},
                }
            };

            return .{
                .reader = reader,
                .seekable_stream = seekable_stream,
                .root_dir = std.mem.bytesToValue(
                    DirectoryRecord.Header,
                    pvd.root_dir[0..@sizeOf(DirectoryRecord.Header)],
                ),
                .block_size = pvd.block_size.lsb,
            };
        }

        pub fn openRoot(self: *const @This()) !File(@This()) {
            return File(@This()).init(self, self.root_dir);
        }
    };
}

pub fn mount(reader: anytype, seekable_stream: anytype) !Mount(@TypeOf(reader), @TypeOf(seekable_stream)) {
    return Mount(@TypeOf(reader), @TypeOf(seekable_stream)).init(reader, seekable_stream);
}
