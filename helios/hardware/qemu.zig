const port = @import("port.zig");
const std = @import("std");

pub const debug_con = std.io.Writer(void, error{}, debugConWrite){
    .context = {},
};

fn debugConWrite(_: void, bytes: []const u8) !usize {
    for (bytes) |byte|
        port.outb(0xe9, byte);
    return bytes.len;
}
