pub const acpi = @import("acpi.zig");
pub const boot = @import("boot.zig");
pub const fs = @import("fs.zig");
pub const hardware = @import("hardware.zig");
pub const memory = @import("memory.zig");
pub const Framebuffer = @import("Framebuffer.zig");
pub const MemoryMap = @import("MemoryMap.zig");
pub const Terminal = @import("Terminal.zig");

pub const QueriedMode = struct {
    width: u32,
    height: u32,
    idx: u32,
};
