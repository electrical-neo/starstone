pub const PageFlags = struct {
    read_write: bool = true,
    cache_disable: bool = false,

    fn asInt(self: @This()) usize {
        var x: usize = 1;
        if (self.read_write)
            x |= 1 << 1;
        if (self.cache_disable)
            x |= 1 << 4;
        return x;
    }
};

fn makeAddress(pml4i: usize, pdpti: usize, pdi: usize, pti: usize) usize {
    return ((pml4i >> 8) * 0xffff000000000000) |
        (pml4i << 39) |
        (pdpti << 30) |
        (pdi << 21) |
        (pti << 12);
}

pub fn mapPage(comptime pmm: anytype, phys: usize, virt: usize, flags: PageFlags) !*align(4096) [4096]u8 {
    if (phys & 0xfff != 0)
        return error.InvalidPhysicalAddress;
    if (virt & 0xfff != 0)
        return error.InvalidVirtualAddress;

    const pti = (virt >> 12) & 0x1ff;
    const pdi = (virt >> 21) & 0x1ff;
    const pdpti = (virt >> 30) & 0x1ff;
    const pml4i = (virt >> 39) & 0x1ff;

    const pml4_entry: *u64 = @ptrFromInt(makeAddress(511, 511, 511, 511) + pml4i * 8);
    if (pml4_entry.* & 1 == 0) {
        const table = try pmm.requestPageFrame();
        pml4_entry.* = table | 3;
        @memset(@as([*]u8, @ptrFromInt(makeAddress(511, 511, 511, pml4i)))[0..4096], 0);
    }

    const pdpt_entry: *u64 = @ptrFromInt(makeAddress(511, 511, 511, pml4i) + pdpti * 8);
    if (pdpt_entry.* & 1 == 0) {
        const table = try pmm.requestPageFrame();
        pdpt_entry.* = table | 3;
        @memset(@as([*]u8, @ptrFromInt(makeAddress(511, 511, pml4i, pdpti)))[0..4096], 0);
    }

    const pd_entry: *u64 = @ptrFromInt(makeAddress(511, 511, pml4i, pdpti) + pdi * 8);
    if (pd_entry.* & 1 == 0) {
        const table = try pmm.requestPageFrame();
        pd_entry.* = table | 3;
        @memset(@as([*]u8, @ptrFromInt(makeAddress(511, pml4i, pdpti, pdi)))[0..4096], 0);
    }

    const pt_entry: *u64 = @ptrFromInt(makeAddress(511, pml4i, pdpti, pdi) + pti * 8);
    asm volatile ("invlpg (%[v_addr])"
        :
        : [v_addr] "r" (virt),
        : "memory"
    );
    pt_entry.* = phys | flags.asInt();

    return @ptrFromInt(virt);
}

pub fn mapRange(comptime pmm: anytype, phys: u64, virt: u64, size: u64) ![]u8 {
    const start = phys & ~@as(u64, 4095);
    const page_count = (phys + size + 4095) / 4096 - start / 4096;

    for (0..page_count) |i| {
        _ = try mapPage(pmm, start + i * 4096, virt + i * 4096);
    }

    return @as([*]u8, @ptrFromInt(virt + (phys - start)))[0..size];
}
