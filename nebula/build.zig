const std = @import("std");

const x86_64_freestanding_target = blk: {
    var sub_features = std.Target.Cpu.Feature.Set.empty;
    var add_features = std.Target.Cpu.Feature.Set.empty;
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.mmx));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.sse));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.sse2));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.avx));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.avx2));
    add_features.addFeature(@intFromEnum(std.Target.x86.Feature.soft_float));
    break :blk std.zig.CrossTarget{
        .cpu_arch = std.Target.Cpu.Arch.x86_64,
        .os_tag = std.Target.Os.Tag.freestanding,
        .abi = std.Target.Abi.none,
        .cpu_features_sub = sub_features,
        .cpu_features_add = add_features,
    };
};

pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});

    const stage3 = b.addModule("stage3", .{
        .source_file = .{ .path = "src/stage3.zig" },
    });

    const helios = b.addModule("helios", .{
        .source_file = .{ .path = "../helios/helios.zig" },
    });

    stage3.dependencies.put("helios", helios) catch @panic("OOM");

    // BIOS/CD
    const bios_cd_elf = b.addExecutable(.{
        .name = "bios_cd",
        .root_source_file = .{ .path = "src/bios/stage2.zig" },
        .target = x86_64_freestanding_target,
        .optimize = .ReleaseSmall,
    });

    bios_cd_elf.strip = true;
    bios_cd_elf.setLinkerScript(.{ .path = "src/bios/cd.ld" });
    bios_cd_elf.addAssemblyFile(.{ .path = "src/bios/cd.S" });
    bios_cd_elf.addAssemblyFile(.{ .path = "src/bios/real_mode.S" });
    bios_cd_elf.addAssemblyFile(.{ .path = "src/kernelEntry.S" });
    bios_cd_elf.addModule("stage3", stage3);
    bios_cd_elf.addModule("helios", helios);

    const bios_cd = b.addObjCopy(
        bios_cd_elf.getEmittedBin(),
        .{ .format = .bin },
    );

    // UEFI/CD
    const uefi_cd_exe = b.addExecutable(.{
        .name = "uefi_cd",
        .root_source_file = .{ .path = "src/uefi/cd.zig" },
        .target = std.zig.CrossTarget{
            .cpu_arch = std.Target.Cpu.Arch.x86_64,
            .os_tag = std.Target.Os.Tag.uefi,
            .abi = std.Target.Abi.msvc,
        },
        .optimize = optimize,
    });

    uefi_cd_exe.strip = true;
    uefi_cd_exe.setLinkerScript(.{ .path = "src/uefi/linker.ld" });
    uefi_cd_exe.addModule("stage3", stage3);
    uefi_cd_exe.addModule("helios", helios);
    uefi_cd_exe.addAssemblyFile(.{ .path = "src/kernelEntry.S" });

    var uefi_cd_step: *std.Build.Step = undefined;
    const uefi_cd = blk: {
        const img_cmd = b.addSystemCommand(&[_][]const u8{ "dd", "if=/dev/zero", "bs=8k", "count=180" });
        const img = img_cmd.addPrefixedOutputFileArg("of=", "uefi_cd");

        const mformat_cmd = b.addSystemCommand(&[_][]const u8{ "mformat", "-i" });
        mformat_cmd.addFileArg(img);
        mformat_cmd.addArg("::");
        mformat_cmd.has_side_effects = true;

        const mmd_efi_cmd = b.addSystemCommand(&[_][]const u8{ "mmd", "-i" });
        mmd_efi_cmd.addFileArg(img);
        mmd_efi_cmd.addArg("::/EFI");
        mmd_efi_cmd.step.dependOn(&mformat_cmd.step);
        mmd_efi_cmd.has_side_effects = true;

        const mmd_efi_boot_cmd = b.addSystemCommand(&[_][]const u8{ "mmd", "-i" });
        mmd_efi_boot_cmd.addFileArg(img);
        mmd_efi_boot_cmd.addArg("::/EFI/BOOT");
        mmd_efi_boot_cmd.step.dependOn(&mmd_efi_cmd.step);
        mmd_efi_boot_cmd.has_side_effects = true;

        const mcopy_cmd = b.addSystemCommand(&[_][]const u8{ "mcopy", "-i" });
        mcopy_cmd.addFileArg(img);
        mcopy_cmd.addFileArg(uefi_cd_exe.getEmittedBin());
        mcopy_cmd.addArg("::/EFI/BOOT/BOOTX64.EFI");
        mcopy_cmd.step.dependOn(&mmd_efi_boot_cmd.step);
        mcopy_cmd.has_side_effects = true;

        uefi_cd_step = &mcopy_cmd.step;
        break :blk img;
    };

    // MKISO
    const mkiso = b.addExecutable(.{
        .name = "nebula-mkiso",
        .root_source_file = .{ .path = "src/mkiso.zig" },
        .optimize = optimize,
    });
    mkiso.addAnonymousModule("bios.cd", .{
        .source_file = bios_cd.getOutput(),
    });
    mkiso.addAnonymousModule("uefi.cd", .{
        .source_file = uefi_cd,
    });
    mkiso.addAnonymousModule("font", .{
        .source_file = .{ .path = "zap-light16.psf" },
    });
    mkiso.step.dependOn(uefi_cd_step);

    b.installArtifact(mkiso);
}
