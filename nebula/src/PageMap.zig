const std = @import("std");

// this will probably need a rewrite
// i just wrote this code
// this code works
// however, i have no clue how this code works
// neither did i have any clue while writing it

const Pt = *align(4096) [512]u64;
const Pd = struct { entries: [512]?Pt, mapped: *align(4096) [512]u64 };
const Pdpt = struct { entries: [512]?*Pd, mapped: *align(4096) [512]u64 };
const Pml4 = [512]?*Pdpt;

allocator: std.mem.Allocator,
requestPageFrame: *const fn () anyerror!u64,
mapPageToAnyVirt: *const fn (u64) anyerror!*align(4096) [4096]u8,
pml4: Pml4,
phys_pml4: u64,
mapped_pml4: *align(4096) [512]u64,

pub fn init(
    allocator: std.mem.Allocator,
    requestPageFrame: *const fn () anyerror!u64,
    mapPageToAnyVirt: *const fn (u64) anyerror!*align(4096) [4096]u8,
) !@This() {
    const phys_pml4 = try requestPageFrame();
    const mapped_pml4 = try mapPageToAnyVirt(phys_pml4);
    @memset(mapped_pml4, 0);

    var self = @This(){
        .allocator = allocator,
        .requestPageFrame = requestPageFrame,
        .mapPageToAnyVirt = mapPageToAnyVirt,
        .pml4 = .{null} ** 512,
        .phys_pml4 = phys_pml4,
        .mapped_pml4 = @ptrCast(mapped_pml4),
    };

    self.pml4[511] = @ptrFromInt(0xfffffffffffff000);
    self.mapped_pml4[511] = self.phys_pml4 | 3;

    return self;
}

pub fn mapPage(self: *@This(), phys: u64, virt: u64) !void {
    if (phys & 0xfff != 0)
        return error.InvalidPhysicalAddress;
    if (virt & 0xfff != 0)
        return error.InvalidVirtualAddress;

    const pti = (virt >> 12) & 0x1ff;
    const pdi = (virt >> 21) & 0x1ff;
    const pdpti = (virt >> 30) & 0x1ff;
    const pml4i = (virt >> 39) & 0x1ff;

    if (self.pml4[pml4i] == null) {
        const phys_table = try self.requestPageFrame();
        const mapped_table = try self.mapPageToAnyVirt(phys_table);
        @memset(mapped_table, 0);

        self.pml4[pml4i] = try self.allocator.create(Pdpt);
        self.pml4[pml4i].?.mapped = @ptrCast(mapped_table);
        self.mapped_pml4[pml4i] = phys_table | 3;
        @memset(&self.pml4[pml4i].?.entries, null);
    }
    const pdpt = self.pml4[pml4i].?;

    if (pdpt.entries[pdpti] == null) {
        const phys_table = try self.requestPageFrame();
        const mapped_table = try self.mapPageToAnyVirt(phys_table);
        @memset(mapped_table, 0);

        pdpt.entries[pdpti] = try self.allocator.create(Pd);
        pdpt.entries[pdpti].?.mapped = @ptrCast(mapped_table);
        pdpt.mapped[pdpti] = phys_table | 3;
        @memset(&pdpt.entries[pdpti].?.entries, null);
    }
    const pd = pdpt.entries[pdpti].?;

    if (pd.entries[pdi] == null) {
        const phys_table = try self.requestPageFrame();
        const mapped_table = try self.mapPageToAnyVirt(phys_table);
        @memset(mapped_table, 0);

        pd.entries[pdi] = @ptrCast(mapped_table);
        pd.mapped[pdi] = phys_table | 3;
    }
    const pt = pd.entries[pdi].?;

    pt[pti] = phys | 3;
}

pub fn mapRange(self: *@This(), phys: u64, size: u64, virt: u64) !u64 {
    const start = phys & ~@as(u64, 4095);
    const page_count = (phys + size + 4095) / 4096 - start / 4096;

    for (0..page_count) |i| {
        try self.mapPage(start + i * 4096, virt + i * 4096);
    }

    return virt + (phys - start);
}
