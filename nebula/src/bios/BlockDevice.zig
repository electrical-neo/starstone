const std = @import("std");
const real_mode = @import("real_mode.zig");

const Dap = extern struct {
    size: u8 = 0x10,
    unused: u8 = 0,
    sectors: u16 = 1,
    offset: u16,
    segment: u16,
    lba: u64,
};

id: u8,
bytes_per_sector: u32,
buffer: []u8,

pub fn init(allocator: std.mem.Allocator, id: u8, bytes_per_sector: u16) !@This() {
    return .{
        .id = id,
        .bytes_per_sector = bytes_per_sector,
        .buffer = try allocator.alloc(u8, bytes_per_sector),
    };
}

pub fn readSector(self: @This(), lba: u64) !void {
    if (lba > 0xffffffffffff)
        return error.InvalidLba;

    const dap = Dap{
        .offset = try real_mode.ptrOffset(&self.buffer[0]),
        .segment = try real_mode.ptrSegment(&self.buffer[0]),
        .lba = lba,
    };

    var regs = real_mode.Registers{};
    regs.eax = 0x4200;
    regs.edx = self.id;
    regs.ds = try real_mode.ptrSegment(&dap);
    regs.esi = try real_mode.ptrOffset(&dap);
    real_mode.int(0x13, &regs);

    if (regs.eflags & real_mode.Registers.eflags_carry != 0)
        return error.DiskReadError;
}
