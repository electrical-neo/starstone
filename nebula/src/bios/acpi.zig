const helios = @import("helios");

fn getEbda() u32 {
    const ebda = @as(*u16, @ptrFromInt(0x40e)).* << 4;
    if (ebda < 0x80000 or ebda >= 0xa0000)
        return 0x80000;
    return ebda;
}

pub fn findSdp() ?helios.acpi.Sdp {
    const ebda = getEbda();
    var i = ebda;
    return while (i < 0x100000) : (i += 16) {
        if (i == ebda + 1024)
            i = 0xe0000;

        const rsdp: *align(1) const helios.acpi.Rsdp = @ptrFromInt(i);
        if (!rsdp.validate())
            continue;

        if (rsdp.revision == 2) {
            const xsdp: *align(1) const helios.acpi.Xsdp = @ptrCast(rsdp);
            if (xsdp.validate())
                break .{ .xsdp = xsdp };
        }

        break .{ .rsdp = rsdp };
    } else null;
}
