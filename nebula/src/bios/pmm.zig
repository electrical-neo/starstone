const std = @import("std");
const helios = @import("helios");
const real_mode = @import("real_mode.zig");

const BiosMemoryMapEntry = extern struct {
    const Type = enum(u32) {
        usable = 1,
        reserved,
        acpi_reclaimable,
        acpi_nvs,
        bad_memory,
        _,
    };

    base: u64,
    len: u64,
    type: Type,
};

pub var mmap: ?helios.MemoryMap = null;

pub fn init(allocator: std.mem.Allocator) !void {
    var bios_mmap = std.ArrayList(BiosMemoryMapEntry).init(allocator);
    defer bios_mmap.deinit();

    var bios_mmap_entry: BiosMemoryMapEntry = undefined;

    var regs = real_mode.Registers{};
    regs.ebx = 0;
    regs.ecx = @sizeOf(BiosMemoryMapEntry);
    regs.edx = 0x534d4150;
    regs.es = try real_mode.ptrSegment(&bios_mmap_entry);
    regs.edi = try real_mode.ptrOffset(&bios_mmap_entry);

    var mem_cap: u64 = 0;

    while (true) {
        regs.eax = 0xe820;
        real_mode.int(0x15, &regs);

        if (regs.eax != 0x534d4150)
            return error.NoMemoryMap;

        if (regs.eflags & real_mode.Registers.eflags_carry != 0)
            break;

        try bios_mmap.append(bios_mmap_entry);
        if (mem_cap < bios_mmap_entry.base + bios_mmap_entry.len)
            mem_cap = bios_mmap_entry.base + bios_mmap_entry.len;

        if (regs.ebx == 0)
            break;
    }

    mmap = try helios.MemoryMap.init(allocator, mem_cap);

    for (bios_mmap.items) |e| {
        try mmap.?.restrictArea(e.base, e.len, switch (e.type) {
            .usable => .free,
            .acpi_reclaimable => .acpi_reclaimable,
            else => .reserved,
        });
    }

    try mmap.?.restrictArea(0, 0x100000, .low_memory);

    mmap.?.reserveHoles();
}

pub fn printMemoryMap(stdout: anytype) !void {
    var chunk: ?*helios.MemoryMap.MemoryChunk = (mmap orelse return error.NoMemoryMap).head.next;
    var free_mem: u64 = 0;
    var chunk_base: u64 = 0;
    try stdout.print("BASE               | LENGTH             | TYPE\n", .{});
    while (chunk) |c| {
        defer chunk = c.next;
        defer chunk_base += c.len;

        try stdout.print("0x{x:0>16} | 0x{x:0>16} | {s}\n", .{
            chunk_base,
            c.len,
            @tagName(c.type),
        });

        if (c.type == .free)
            free_mem += c.len;
    }

    try stdout.print("Free memory: {} MB\n", .{free_mem / 1024 / 1024});
}

pub fn requestPageFrame() !u64 {
    return (mmap orelse return error.NoMemoryMap).requestPageFrame();
}
