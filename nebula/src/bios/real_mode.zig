pub const Registers = extern struct {
    pub const eflags_carry = 1 << 0;

    eax: u32 = 0,
    ebx: u32 = 0,
    ecx: u32 = 0,
    edx: u32 = 0,
    esi: u32 = 0,
    edi: u32 = 0,
    ebp: u32 = 0,
    eflags: u32 = 0,
    ds: u16 align(1) = 0,
    es: u16 align(1) = 0,
};

pub extern fn int(no: u8, regs: *Registers) callconv(.C) void;

pub fn ptrOffset(ptr: anytype) !u16 {
    const ptr_int = @intFromPtr(ptr);
    if (ptr_int > 0x100000)
        return error.AddressTooHigh;
    return @intCast(ptr_int & 0xf);
}

pub fn ptrSegment(ptr: anytype) !u16 {
    const ptr_int = @intFromPtr(ptr);
    if (ptr_int > 0x100000)
        return error.AddressTooHigh;
    return @intCast(ptr_int >> 4);
}

pub fn ptrFromSegmentOffset(comptime Ptr: type, segment: u16, offset: u16) Ptr {
    return @ptrFromInt(segment * 0x10 + offset);
}
