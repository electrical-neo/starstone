const std = @import("std");
const stage3 = @import("stage3");
const vga = @import("vga.zig");
const real_mode = @import("real_mode.zig");
const BlockDevice = @import("BlockDevice.zig");
const pmm = @import("pmm.zig");
const vmm = @import("vmm.zig");
const vesa = @import("vesa.zig");
const acpi = @import("acpi.zig");
const helios = @import("helios");

var fin_panic: ?*const fn ([]const u8) noreturn = null;
pub fn panic(msg: []const u8, error_return_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    _ = error_return_trace;
    _ = ret_addr;

    if (fin_panic) |p| {
        p(msg);
    } else {
        vga.init();
        vga.writer.print("panic: {s}", .{msg}) catch {};

        while (true)
            asm volatile ("hlt");
    }
}

export fn zigStart(boot_drive: u8) callconv(.C) noreturn {
    main(boot_drive) catch |err| @panic(@errorName(err));
}

fn finalizeBoot(new_panic: *const fn ([]const u8) noreturn) !helios.MemoryMap {
    fin_panic = new_panic;
    return pmm.mmap.?;
}

var fba_buf: [200 * 1024]u8 = undefined;

fn main(boot_drive: u8) !noreturn {
    var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
    const allocator = fba.allocator();

    const boot_device = try BlockDevice.init(allocator, boot_drive, 2048);

    try pmm.init(allocator);
    try vesa.init(allocator);

    stage3.main(
        boot_device,
        allocator,
        pmm.requestPageFrame,
        vmm.mapPageToAnyVirt,
        vmm.mapRangeToAnyVirt,
        vesa.queryNextVideoMode,
        vesa.setVideoMode,
        finalizeBoot,
        acpi.findSdp() orelse @panic("no RSDP/XSDP"),
    );
}
