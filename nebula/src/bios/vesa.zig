const std = @import("std");
const real_mode = @import("real_mode.zig");
const stage3 = @import("stage3");
const helios = @import("helios");

pub const VbeInfo = extern struct {
    signature: [4]u8 align(1),
    version: u16 align(1),
    oem_str_offset: u16 align(1),
    oem_str_segment: u16 align(1),
    capabilities: u32 align(1),
    video_mode_ptr_offset: u16 align(1),
    video_mode_ptr_segment: u16 align(1),
    total_memory: u16 align(1), // 64k blocks
    reserved: [492]u8 align(1),
};

pub const ModeInfo = extern struct {
    attributes: u16 align(1),
    window_a: u8 align(1),
    window_b: u8 align(1),
    granularity: u16 align(1),
    window_size: u16 align(1),
    segment_a: u16 align(1),
    segment_b: u16 align(1),
    win_func_ptr: u32 align(1),
    pitch: u16 align(1),
    width: u16 align(1),
    height: u16 align(1),
    w_char: u8 align(1),
    y_char: u8 align(1),
    planes: u8 align(1),
    bpp: u8 align(1),
    banks: u8 align(1),
    memory_model: u8 align(1),
    bank_size: u8 align(1),
    image_pages: u8 align(1),
    reserved0: u8 align(1),

    red_mask: u8 align(1),
    red_position: u8 align(1),
    green_mask: u8 align(1),
    green_position: u8 align(1),
    blue_mask: u8 align(1),
    blue_position: u8 align(1),
    reserved_mask: u8 align(1),
    reserved_position: u8 align(1),
    direct_color_attributes: u8 align(1),

    framebuffer: u32 align(1),
    off_screen_mem_off: u32 align(1),
    off_screen_mem_size: u16 align(1),
    reserved1: [206]u8 align(1),

    pub fn hasLfb(self: *const @This()) bool {
        return self.attributes & (1 << 7) != 0;
    }
};

fn getInfo() !VbeInfo {
    var info: VbeInfo = undefined;
    info.signature = [_]u8{ 'V', 'B', 'E', '2' };

    var regs: real_mode.Registers = .{
        .eax = 0x4f00,
        .es = try real_mode.ptrSegment(&info),
        .edi = try real_mode.ptrOffset(&info),
    };

    real_mode.int(0x10, &regs);

    if (regs.eax & 0xffff != 0x4f)
        return error.VesaError;

    if (!std.mem.eql(u8, &info.signature, "VESA"))
        return error.VesaError;

    return info;
}

pub fn getModeInfo(mode: u16) !ModeInfo {
    var info: ModeInfo = undefined;

    var regs: real_mode.Registers = .{
        .eax = 0x4f01,
        .ecx = mode,
        .es = try real_mode.ptrSegment(&info),
        .edi = try real_mode.ptrOffset(&info),
    };

    real_mode.int(0x10, &regs);

    if (regs.eax & 0xffff != 0x4f)
        return error.VesaError;

    return info;
}

pub var video_modes: ?[]u16 = null;
pub var video_mode: u16 linksection(".data") = 0;

pub fn init(allocator: std.mem.Allocator) !void {
    const vbe = try getInfo();
    const video_modes_vesa = std.mem.span(real_mode.ptrFromSegmentOffset(
        [*:0xffff]u16,
        vbe.video_mode_ptr_segment,
        vbe.video_mode_ptr_offset,
    ));

    video_modes = try allocator.alloc(u16, video_modes_vesa.len);
    @memcpy(video_modes.?, video_modes_vesa);
}

pub fn queryNextVideoMode() anyerror!?helios.QueriedMode {
    var info: ModeInfo = undefined;
    var idx: u32 = undefined;
    if (video_modes) |vm| {
        while (true) {
            if (video_mode >= vm.len) {
                video_mode = 0;
                return null;
            }

            info = try getModeInfo(vm[video_mode]);

            idx = vm[video_mode];
            video_mode += 1;

            if (info.bpp == 24 or info.bpp == 32)
                break;
        }

        return .{
            .width = info.width,
            .height = info.height,
            .idx = idx,
        };
    } else return error.NoVideoModes;
}

pub fn setVideoMode(idx: u32) anyerror!helios.Framebuffer {
    const info = try getModeInfo(@intCast(idx));

    var regs: real_mode.Registers = .{
        .eax = 0x4f02,
        .ebx = idx,
    };
    real_mode.int(0x10, &regs);
    if (regs.eax & 0xffff != 0x4f)
        return error.VesaError;

    return .{
        .base = info.framebuffer,
        .width = info.width,
        .height = info.height,
        .pitch = info.pitch,
        .pixel_size = info.bpp / 8,
    };
}
