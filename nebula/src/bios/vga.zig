const std = @import("std");
const real_mode = @import("real_mode.zig");

const Character = packed struct {
    code_point: u8,
    color: u8 = 0x1f,
};
const Writer = std.io.Writer(void, error{}, write);

const buffer: *[25][80]Character = @ptrFromInt(0xb8000);
pub const writer: Writer = .{ .context = {} };

var cursor_x: u8 = undefined;
var cursor_y: u8 = undefined;

pub fn init() void {
    var regs: real_mode.Registers = .{ .eax = 2 };
    real_mode.int(0x10, &regs);
    regs.eax = 0x100;
    regs.ecx = 0x3f00;
    real_mode.int(0x10, &regs);

    for (buffer) |*row| {
        for (row) |*cell| {
            cell.* = .{ .code_point = ' ' };
        }
    }

    cursor_x = 0;
    cursor_y = 0;
}

fn lineFeed() void {
    cursor_x = 0;
    cursor_y += 1;

    if (cursor_y >= 25)
        scroll();
}

fn scroll() void {
    @memcpy(buffer[0..24], buffer[1..25]);
    @memset(&buffer[24], .{ .code_point = ' ' });
    cursor_y -= 1;
}

fn putc(c: u8) void {
    if (c == '\n') {
        lineFeed();
        return;
    }

    if (cursor_x >= 80)
        lineFeed();

    buffer[cursor_y][cursor_x].code_point = c;
    cursor_x += 1;
}

fn write(_: void, bytes: []const u8) error{}!usize {
    for (bytes) |byte|
        putc(byte);
    return bytes.len;
}
