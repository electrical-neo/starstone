const std = @import("std");
const helios = @import("helios");
const pmm = @import("pmm.zig");

var free_address: u64 = 0x101000;

pub fn mapPageToAnyVirt(phys: usize) !*align(4096) [4096]u8 {
    const v = try helios.memory.vmm.mapPage(pmm, phys, free_address);
    free_address += 4096;
    return v;
}

pub fn mapRangeToAnyVirt(phys: u64, size: u64) !u64 {
    const start = phys & ~@as(u64, 4095);
    const page_count = (phys + size + 4095) / 4096 - start / 4096;
    defer free_address += page_count * 4096;
    _ = try helios.memory.vmm.mapRange(pmm, phys, free_address, size);
    return free_address + (phys - start);
}
