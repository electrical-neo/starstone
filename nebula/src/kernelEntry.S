.global kernelEntry

.section .aligned

// IN rdi - pml4
// IN rsi - kernel entry point
kernelEntry:
	mov %rdi, %cr3
	mov $0x1234, %rax
	jmp *%rsi
