const std = @import("std");

const nebula = struct {
    const bios = struct {
        const cd = @embedFile("bios.cd");
    };
    const uefi = struct {
        const cd = @embedFile("uefi.cd");
    };
};

const font = @embedFile("font");

const Cli = struct {
    allocator: std.mem.Allocator,
    source: []const u8,
    output: []const u8,
    volid: ?[]const u8,

    fn fromArgs(allocator: std.mem.Allocator) !@This() {
        var args = try std.process.argsWithAllocator(allocator);
        defer args.deinit();

        const exe_name = args.next() orelse return error.NoExecutableName;

        var output: ?[]const u8 = null;
        var source: ?[]const u8 = null;
        var volid: ?[]const u8 = null;
        while (args.next()) |arg| {
            if (std.mem.eql(u8, arg, "-h") or std.mem.eql(u8, arg, "--help")) {
                var stdout = std.io.getStdOut();
                const writer = stdout.writer();

                try writer.print(
                    "usage: {s} (-h | --help) (-v | --volid ID) [-o | --output FILE] SOURCE\n",
                    .{exe_name},
                );

                std.process.exit(0);
            }

            if (std.mem.eql(u8, arg, "-o") or std.mem.eql(u8, arg, "--output")) {
                if (output != null) {
                    return error.MultipleOutputs;
                }

                output = args.next() orelse return error.ExpectedOutput;
                continue;
            }

            if (std.mem.eql(u8, arg, "-V") or std.mem.eql(u8, arg, "--volid")) {
                if (volid != null) {
                    return error.MultipleVolumeIds;
                }

                volid = args.next() orelse return error.ExpectedVolumeId;
                continue;
            }

            if (arg[0] == '-')
                return error.UnknownOption;

            if (source != null) {
                return error.MultipleSources;
            }

            source = arg;
        }

        if (output == null) {
            return error.NoOutput;
        }

        if (source == null) {
            return error.NoSource;
        }

        return .{
            .allocator = allocator,
            .output = try allocator.dupe(u8, output.?),
            .source = try allocator.dupe(u8, source.?),
            .volid = if (volid) |v| try allocator.dupe(u8, v) else null,
        };
    }

    fn deinit(self: *@This()) void {
        self.allocator.free(self.source);
        self.allocator.free(self.output);
        if (self.volid) |v|
            self.allocator.free(v);
        self.* = undefined;
    }
};

fn openDir(path: []const u8) !std.fs.Dir {
    if (std.fs.path.isAbsolute(path))
        return std.fs.openDirAbsolute(path, .{});
    return std.fs.cwd().openDir(path, .{});
}

fn installFile(prefix: []const u8, dir_path: []const u8, file_name: []const u8, file_bytes: []const u8) !void {
    var prefix_dir = try openDir(prefix);
    defer prefix_dir.close();

    try prefix_dir.makePath(dir_path);
    var dir = try prefix_dir.openDir(dir_path, .{});
    defer dir.close();

    var file = try dir.createFile(file_name, .{});
    defer file.close();

    var writer = file.writer();
    try writer.writeAll(file_bytes);
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer {
        if (gpa.deinit() == .leak) {
            std.debug.print("Memory leak occured!\n", .{});
        }
    }

    var args = try Cli.fromArgs(allocator);
    defer args.deinit();

    try installFile(args.source, "boot/nebula/eltorito", "bios", nebula.bios.cd);
    try installFile(args.source, "boot/nebula/eltorito", "uefi", nebula.uefi.cd);
    try installFile(args.source, "boot/nebula", "font", font);

    var xorriso = std.ChildProcess.init(&[_][]const u8{
        "xorriso",
        "-as",
        "mkisofs",
        "-V",
        args.volid orelse "NEBULA",

        "-c",
        "boot/boot.cat",

        "-b",
        "boot/nebula/eltorito/bios",
        "-boot-info-table",
        "-boot-load-size",
        "4",
        "-no-emul-boot",

        "-eltorito-alt-boot",

        "-e",
        "boot/nebula/eltorito/uefi",
        "-no-emul-boot",

        args.source,
        "-o",
        args.output,
    }, allocator);
    switch (try xorriso.spawnAndWait()) {
        .Exited => |code| {
            if (code != 0) {
                return error.XorrisoError;
            }
        },
        else => return error.XorrisoError,
    }
}
