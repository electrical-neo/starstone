const std = @import("std");
const helios = @import("helios");
const PageMap = @import("PageMap.zig");

fn Disk(comptime BlockDevice: type) type {
    return struct {
        const Reader = std.io.Reader(*@This(), anyerror, read);
        const SeekableStream = std.io.SeekableStream(*@This(), anyerror, anyerror, seekTo, seekBy, getPos, getEndPos);

        currently_loaded_lba: ?u64 = null,
        seek: u64 = 0,
        block_device: BlockDevice,

        fn init(block_device: BlockDevice) @This() {
            return .{
                .block_device = block_device,
            };
        }

        fn loadBlock(self: *@This()) !void {
            const seek_lba = self.seek / self.block_device.bytes_per_sector;
            if (self.currently_loaded_lba == seek_lba)
                return;

            try self.block_device.readSector(seek_lba);
            self.currently_loaded_lba = seek_lba;
        }

        fn read(self: *@This(), bytes: []u8) anyerror!usize {
            var read_bytes: u64 = 0;
            var left_to_read: u64 = bytes.len;

            while (left_to_read > 0) {
                self.loadBlock() catch return read_bytes;
                const seek_in_block = self.seek % self.block_device.bytes_per_sector;
                const to_copy = @min(
                    left_to_read,
                    self.block_device.bytes_per_sector - seek_in_block,
                );
                @memcpy(bytes[read_bytes .. read_bytes + to_copy], self.block_device.buffer[seek_in_block .. seek_in_block + to_copy]);

                read_bytes += to_copy;
                left_to_read -= to_copy;
                self.seek += to_copy;
            }

            return read_bytes;
        }

        fn seekTo(self: *@This(), pos: u64) anyerror!void {
            self.seek = pos;
        }

        fn seekBy(self: *@This(), amt: i64) anyerror!void {
            if (amt < 0) {
                if (self.seek < -amt)
                    return error.Unseekable;
                self.seek -= @as(u64, @intCast(-amt));
            } else {
                if (self.seek +% @as(u64, @intCast(amt)) != self.seek +| @as(u64, @intCast(amt)))
                    return error.Unseekable;
                self.seek += @as(u64, @intCast(amt));
            }
        }

        fn getPos(self: *@This()) anyerror!u64 {
            return self.seek;
        }

        fn getEndPos(self: *@This()) anyerror!u64 {
            _ = self;
            return error.NoDiskInfo;
        }

        fn reader(self: *@This()) Reader {
            return .{ .context = self };
        }

        fn seekableStream(self: *@This()) SeekableStream {
            return .{ .context = self };
        }
    };
}

fn diskFromBlockDevice(block_device: anytype) Disk(@TypeOf(block_device)) {
    return Disk(@TypeOf(block_device)).init(block_device);
}

const Psf1Header = extern struct {
    const magic = 0x0436;

    magic: u16 align(1),
    mode: u8 align(1),
    char_size: u8 align(1),
};

extern fn kernelEntry(pml4: u64, entry: u64) callconv(.SysV) noreturn;

var framebuffer: ?helios.Framebuffer = null;
var font_bytes: ?*align(4096) [4096]u8 = null;
fn panic(msg: []const u8) noreturn {
    if (framebuffer != null and font_bytes != null) {
        var terminal = helios.Terminal{ .fb = framebuffer.?, .font = @ptrCast(font_bytes.?) };
        terminal.clear();
        const stdout = terminal.writer();
        stdout.print("panic: {s}", .{msg}) catch {};
    }

    while (true) asm volatile ("hlt");
}

pub fn main(
    boot_device: anytype,
    allocator: std.mem.Allocator,
    requestPageFrame: *const fn () anyerror!u64,
    mapPageToAnyVirt: *const fn (u64) anyerror!*align(4096) [4096]u8,
    mapRangeToAnyVirt: *const fn (u64, u64) anyerror!u64,
    queryNextVideoMode: *const fn () anyerror!?helios.QueriedMode,
    setVideoMode: *const fn (u32) anyerror!helios.Framebuffer,
    finalizeBoot: *const fn (*const fn ([]const u8) noreturn) anyerror!helios.MemoryMap,
    sdp: helios.acpi.Sdp,
) noreturn {
    var disk = diskFromBlockDevice(boot_device);
    const disk_r = disk.reader();
    const disk_sk = disk.seekableStream();

    const mount = helios.fs.iso9660.mount(disk_r, disk_sk) catch |err| std.debug.panic("cannot mount the boot partition: {s}", .{@errorName(err)});
    var dir_root = mount.openRoot() catch |err| std.debug.panic("cannot open `/`: {s}", .{@errorName(err)});
    var dir_boot = dir_root.openFile("BOOT") catch |err| std.debug.panic("cannot open `/BOOT`: {s}", .{@errorName(err)});
    var dir_nebula = dir_boot.openFile("NEBULA") catch |err| std.debug.panic("cannot open `/BOOT/NEBULA`: {s}", .{@errorName(err)});
    var font = dir_nebula.openFile("FONT.;1") catch |err| std.debug.panic("cannot open `/BOOT/NEBULA/FONT.;1`: {s}", .{@errorName(err)});
    const font_r = font.reader();

    const font_header = font_r.readStruct(Psf1Header) catch |err| std.debug.panic("cannot read from `/BOOT/NEBULA/FONT.;1`: {s}", .{@errorName(err)});
    if (font_header.magic != Psf1Header.magic or font_header.char_size != 16)
        @panic("`/BOOT/NEBULA/FONT.;1` is not a recognized font");

    const font_phys = requestPageFrame() catch |err| @panic(@errorName(err));
    font_bytes = mapPageToAnyVirt(font_phys) catch |err| @panic(@errorName(err));
    font_r.readNoEof(font_bytes.?) catch |err| std.debug.panic("cannot read from `/BOOT/NEBULA/FONT.;1`: {s}", .{@errorName(err)});

    var best_mode: ?helios.QueriedMode = null;
    while (queryNextVideoMode() catch |err| std.debug.panic("cannot query video modes: {s}", .{@errorName(err)})) |m| {
        if (best_mode) |b| {
            if (m.width > b.width or (m.width == b.width and m.height > b.height))
                best_mode = m;
        } else best_mode = m;
    }

    if (best_mode == null)
        @panic("no video modes");

    const phys_framebuffer = setVideoMode(best_mode.?.idx) catch |err| std.debug.panic("cannot set video mode: {s}", .{@errorName(err)});

    var starcore = dir_boot.openFile("STARCORE.;1") catch |err| std.debug.panic("cannot open `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
    const starcore_r = starcore.reader();
    const starcore_sk = starcore.seekableStream();

    var page_map = PageMap.init(allocator, requestPageFrame, mapPageToAnyVirt) catch |err| @panic(@errorName(err));
    page_map.mapPage(@intFromPtr(&kernelEntry), @intFromPtr(&kernelEntry)) catch |err| @panic(@errorName(err));

    const ehdr = starcore_r.readStruct(std.elf.Elf64_Ehdr) catch |err| std.debug.panic("cannot read from `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
    if (!std.mem.eql(u8, ehdr.e_ident[0..4], &[_]u8{ 0x7f, 'E', 'L', 'F' }))
        @panic("`/BOOT/STARCORE.1;` is not a valid kernel");
    if (ehdr.e_type != .EXEC)
        @panic("`/BOOT/STARCORE.1;` is not a valid kernel");
    if (ehdr.e_machine != .X86_64)
        @panic("`/BOOT/STARCORE.1;` is not a valid kernel");
    if (ehdr.e_version != 1)
        @panic("`/BOOT/STARCORE.1;` is not a valid kernel");

    for (0..ehdr.e_phnum) |i| {
        starcore_sk.seekTo(ehdr.e_phoff + ehdr.e_phentsize * i) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
        const phdr = starcore_r.readStruct(std.elf.Elf64_Phdr) catch |err| std.debug.panic("cannot read from `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});

        if (phdr.p_type == std.elf.PT_LOAD) {
            const pages = (phdr.p_memsz + 4095) / 4096;
            const bytes_len = phdr.p_filesz;
            starcore_sk.seekTo(phdr.p_offset) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
            for (0..pages) |j| {
                const page = requestPageFrame() catch |err| @panic(@errorName(err));
                const page_virt = mapPageToAnyVirt(page) catch |err| @panic(@errorName(err));
                page_map.mapPage(page, phdr.p_vaddr + j * 0x1000) catch |err| @panic(@errorName(err));

                const to_read = @min(bytes_len, 4096);
                if (to_read != 0)
                    starcore_r.readNoEof(page_virt[0..to_read]) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});

                if (to_read != 4096)
                    @memset(page_virt[to_read..], 0);
            }
        }
    }

    const boot_info_phys = requestPageFrame() catch |err| @panic(@errorName(err));
    const boot_info: *helios.boot.NebulaBootInfo = @ptrCast(mapPageToAnyVirt(boot_info_phys) catch |err| @panic(@errorName(err)));
    @memset(@as([*]u128, @ptrCast(&boot_info.dwarf))[0..helios.boot.dwarf_section_count], 0);
    page_map.mapPage(boot_info_phys, @intFromPtr(helios.boot.boot_info)) catch |err| @panic(@errorName(err));
    page_map.mapPage(font_phys, @intFromPtr(helios.boot.font)) catch |err| @panic(@errorName(err));

    const str_section_off = ehdr.e_shoff + @as(u64, ehdr.e_shentsize) * @as(u64, ehdr.e_shstrndx);
    starcore_sk.seekTo(str_section_off) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
    const str_shdr = starcore_r.readStruct(std.elf.Shdr) catch |err| std.debug.panic("cannot read from `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
    const header_strings = allocator.alloc(u8, str_shdr.sh_size) catch |err| @panic(@errorName(err));
    starcore_sk.seekTo(str_shdr.sh_offset) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
    starcore_r.readNoEof(header_strings) catch |err| std.debug.panic("cannot read from `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});

    var boot_info_var_data: u64 = helios.boot.boot_info_var_data;
    for (0..ehdr.e_shnum) |i| {
        starcore_sk.seekTo(ehdr.e_shoff + ehdr.e_shentsize * i) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
        const shdr = starcore_r.readStruct(std.elf.Elf64_Shdr) catch |err| std.debug.panic("cannot read from `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});

        if (shdr.sh_type == std.elf.SHT_NULL or shdr.sh_type == std.elf.SHT_NOBITS)
            continue;
        const name = std.mem.sliceTo(header_strings[shdr.sh_name..], 0);

        const idx: usize = inline for (@typeInfo(helios.boot.DwarfSection).Enum.fields, 0..) |sect, j| {
            if (std.mem.eql(u8, "." ++ sect.name, name))
                break j;
        } else continue;

        if (boot_info.dwarf[idx].size != 0)
            continue;

        // TODO: decompression
        if (shdr.sh_flags & std.elf.SHF_COMPRESSED != 0)
            continue;

        const pages = (shdr.sh_size + 4095) / 4096;
        const bytes_len = shdr.sh_size;
        starcore_sk.seekTo(shdr.sh_offset) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
        for (0..pages) |j| {
            const page = requestPageFrame() catch |err| @panic(@errorName(err));
            const page_virt = mapPageToAnyVirt(page) catch |err| @panic(@errorName(err));
            page_map.mapPage(page, boot_info_var_data + j * 0x1000) catch |err| @panic(@errorName(err));

            const to_read = @min(bytes_len, 4096);
            if (to_read == 0)
                break;
            starcore_r.readNoEof(page_virt[0..to_read]) catch |err| std.debug.panic("cannot seek in `/BOOT/STARCORE.;1`: {s}", .{@errorName(err)});
        }

        boot_info.dwarf[idx] = .{
            .base = boot_info_var_data,
            .size = shdr.sh_size,
        };
        boot_info_var_data += pages * 0x1000;
    }

    boot_info.framebuffer = .{
        .base = undefined,
        .width = phys_framebuffer.width,
        .height = phys_framebuffer.height,
        .pitch = phys_framebuffer.pitch,
        .pixel_size = phys_framebuffer.pixel_size,
    };
    boot_info.framebuffer.base = page_map.mapRange(phys_framebuffer.base, phys_framebuffer.pitch * phys_framebuffer.height, helios.boot.framebuffer_page_start) catch |err| @panic(@errorName(err));

    switch (sdp) {
        .rsdp => |x| {
            boot_info.rsdt = x.rsdt_addr;
            boot_info.xsdt = 0;
        },
        .xsdp => |x| {
            boot_info.rsdt = 0;
            boot_info.xsdt = x.xdst_addr;
        },
    }

    framebuffer = phys_framebuffer;
    framebuffer.?.base = mapRangeToAnyVirt(phys_framebuffer.base, phys_framebuffer.pitch * phys_framebuffer.height) catch |err| @panic(@errorName(err));

    const kmmap_phys = requestPageFrame() catch |err| @panic(@errorName(err));
    const kmmap: *[512]helios.boot.MemoryMapEntry = @ptrCast(mapPageToAnyVirt(kmmap_phys) catch |err| @panic(@errorName(err)));
    page_map.mapPage(kmmap_phys, @intFromPtr(helios.boot.mmap)) catch |err| @panic(@errorName(err));

    const mmap = finalizeBoot(&panic) catch |err| @panic(@errorName(err));
    boot_info.mmap_entries = mmap.intoNebulaBootMmap(kmmap) catch |err| @panic(@errorName(err));

    kernelEntry(page_map.phys_pml4, ehdr.e_entry);
}
