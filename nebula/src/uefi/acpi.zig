const std = @import("std");
const helios = @import("helios");

pub fn findSdp() ?helios.acpi.Sdp {
    var found_rsdp: ?*align(1) const helios.acpi.Rsdp = null;

    for (0..std.os.uefi.system_table.number_of_table_entries) |i| {
        const table = std.os.uefi.system_table.configuration_table[i];

        if (table.vendor_guid.eql(std.os.uefi.tables.ConfigurationTable.acpi_20_table_guid)) {
            const xsdp: *align(1) const helios.acpi.Xsdp = @ptrCast(table.vendor_table);
            if (xsdp.validate())
                return .{ .xsdp = xsdp };
        } else if (table.vendor_guid.eql(std.os.uefi.tables.ConfigurationTable.acpi_10_table_guid)) {
            const rsdp: *align(1) const helios.acpi.Rsdp = @ptrCast(table.vendor_table);
            if (rsdp.validate())
                found_rsdp = rsdp;
        }
    }

    return if (found_rsdp) |rsdp|
        .{ .rsdp = rsdp }
    else
        null;
}
