const std = @import("std");
const stage2 = @import("stage2.zig");

pub fn panic(msg: []const u8, error_return_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    _ = error_return_trace;
    _ = ret_addr;

    if (stage2.panic) |p| {
        p(msg);
    } else {
        EfiTextOutput.init();
        EfiTextOutput.writer.print("panic: {s}", .{msg}) catch {};
        while (true)
            asm volatile ("hlt");
    }
}

const EfiTextOutput = struct {
    const Writer = std.io.Writer(void, error{}, write);
    const writer: Writer = .{ .context = {} };

    fn init() void {
        const stdout = std.os.uefi.system_table.con_out orelse return;
        _ = stdout.reset(false);
        _ = stdout.enableCursor(false);
        _ = stdout.setAttribute(0x1f);
        _ = stdout.clearScreen();
    }

    fn write(_: void, bytes: []const u8) !usize {
        const stdout = std.os.uefi.system_table.con_out orelse return 0;
        var str: [1:0]u16 = .{undefined};
        for (bytes, 0..) |byte, i| {
            if (byte == '\n')
                _ = stdout.outputString(&[_:0]u16{'\r'}).err() catch return i;

            str[0] = byte;
            _ = stdout.outputString(&str).err() catch return i;
        }
        return bytes.len;
    }
};

pub fn main() std.os.uefi.Status {
    var lip: ?*std.os.uefi.protocols.LoadedImageProtocol = null;
    std.os.uefi.system_table.boot_services.?.handleProtocol(
        std.os.uefi.handle,
        &std.os.uefi.protocols.LoadedImageProtocol.guid,
        @ptrCast(&lip),
    ).err() catch |err| std.debug.panic("failed to handle loaded image protocol: {s}", .{@errorName(err)});

    var dpp: ?*std.os.uefi.protocols.DevicePathProtocol = null;
    std.os.uefi.system_table.boot_services.?.handleProtocol(
        lip.?.device_handle.?,
        &std.os.uefi.protocols.DevicePathProtocol.guid,
        @ptrCast(&dpp),
    ).err() catch |err| std.debug.panic("failed to handle device path protocol: {s}", .{@errorName(err)});

    var d = dpp.?;
    while (d.next()) |p| {
        if (p.type == .Media and p.subtype == 2) {
            d = d.next().?;
            d.type = .End;
            d.subtype = 255;
            d.length = 4;
        }
        d = p;
    }
    d = dpp.?;

    var cd: ?std.os.uefi.Handle = null;
    std.os.uefi.system_table.boot_services.?.locateDevicePath(
        &std.os.uefi.protocols.BlockIoProtocol.guid,
        &d,
        &cd,
    ).err() catch |err| std.debug.panic("failed to locate device path: {s}", .{@errorName(err)});

    stage2.main(cd.?) catch |err| @panic(@errorName(err));
}
