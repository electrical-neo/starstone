const std = @import("std");
const stage3 = @import("stage3");
const helios = @import("helios");

pub var gop: ?*std.os.uefi.protocols.GraphicsOutputProtocol = undefined;

pub fn init() !void {
    try std.os.uefi.system_table.boot_services.?.locateProtocol(
        &std.os.uefi.protocols.GraphicsOutputProtocol.guid,
        null,
        @ptrCast(&gop),
    ).err();
}

fn getModeInfo(mode: u32) !*std.os.uefi.protocols.GraphicsOutputModeInformation {
    if (gop) |g| {
        var size_of_info: usize = undefined;
        var info: *std.os.uefi.protocols.GraphicsOutputModeInformation = undefined;
        try g.queryMode(mode, &size_of_info, &info).err();
        return info;
    } else return error.NoGop;
}

var video_mode: u32 = 0;
pub fn queryNextVideoMode() anyerror!?helios.QueriedMode {
    if (gop) |g| {
        if (video_mode >= g.mode.max_mode) {
            video_mode = 0;
            return null;
        }

        const info = try getModeInfo(video_mode);
        const idx = video_mode;
        video_mode += 1;
        return .{
            .width = info.horizontal_resolution,
            .height = info.vertical_resolution,
            .idx = idx,
        };
    } else return error.NoGop;
}

pub fn setVideoMode(idx: u32) !helios.Framebuffer {
    if (gop) |g| {
        try g.setMode(idx).err();

        return .{
            .base = g.mode.frame_buffer_base,
            .width = g.mode.info.horizontal_resolution,
            .height = g.mode.info.vertical_resolution,
            .pitch = g.mode.info.pixels_per_scan_line * 4,
            .pixel_size = 4,
        };
    } else return error.NoGop;
}
