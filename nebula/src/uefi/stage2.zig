const std = @import("std");
const helios = @import("helios");
const stage3 = @import("stage3");
const acpi = @import("acpi.zig");
const BlockDevice = @import("BlockDevice.zig");
const gop = @import("gop.zig");

fn requestPageFrame() !u64 {
    var buf: [*]align(4096) u8 = undefined;
    try std.os.uefi.system_table.boot_services.?.allocatePages(.AllocateAnyPages, .LoaderData, 1, &buf).err();
    return @intFromPtr(buf);
}

fn mapPageToAnyVirt(phys: usize) !*align(4096) [4096]u8 {
    return @ptrFromInt(phys); // uefi does identity mapping
}

pub var panic: ?*const fn ([]const u8) noreturn = null;
fn finalizeBoot(new_panic: *const fn ([]const u8) noreturn) !helios.MemoryMap {
    var mmap_size: usize = 0;
    var descriptor_size: usize = 0;
    var descriptor_version: u32 = 0;
    var mmap_key: usize = 0;

    _ = std.os.uefi.system_table.boot_services.?.getMemoryMap(&mmap_size, null, &mmap_key, &descriptor_size, &descriptor_version);
    var mmap: [*]align(8) u8 = undefined;
    mmap_size += descriptor_size * 2;
    try std.os.uefi.system_table.boot_services.?.allocatePool(.LoaderData, mmap_size, @ptrCast(&mmap)).err();
    errdefer _ = std.os.uefi.system_table.boot_services.?.freePool(@ptrCast(mmap));

    try std.os.uefi.system_table.boot_services.?.getMemoryMap(&mmap_size, @ptrCast(mmap), &mmap_key, &descriptor_size, &descriptor_version).err();

    var mem_cap: u64 = 0;

    var i: u64 = 0;
    while (i < mmap_size) : (i += descriptor_size) {
        const desc: *align(1) const std.os.uefi.tables.MemoryDescriptor = @ptrCast(&mmap[i]);

        if (mem_cap < desc.physical_start + desc.number_of_pages * 4096)
            mem_cap = desc.physical_start + desc.number_of_pages * 4096;
    }

    var helios_mmap = try helios.MemoryMap.init(allocator, mem_cap);
    i = 0;
    while (i < mmap_size) : (i += descriptor_size) {
        const desc: *align(1) const std.os.uefi.tables.MemoryDescriptor = @ptrCast(&mmap[i]);

        try helios_mmap.restrictArea(desc.physical_start, desc.number_of_pages * 4096, switch (desc.type) {
            .ConventionalMemory => .free,
            .BootServicesCode => .free,
            .BootServicesData => .free,
            .RuntimeServicesCode => .free,
            .RuntimeServicesData => .free,
            .ACPIReclaimMemory => .acpi_reclaimable,
            else => .reserved,
        });
    }

    try helios_mmap.restrictArea(0, 0x100000, .low_memory);

    helios_mmap.reserveHoles();

    try std.os.uefi.system_table.boot_services.?.exitBootServices(std.os.uefi.handle, mmap_key).err();
    panic = new_panic;

    asm volatile ("cli");
    helios.hardware.port.outb(0x70, helios.hardware.port.inb(0x70) | 0x80);
    _ = helios.hardware.port.inb(0x71);

    return helios_mmap;
}

fn mapRangeToAnyVirt(phys: u64, size: u64) !u64 {
    _ = size;
    return phys; // uefi does identity mapping
}

var fba_buf: [200 * 1024]u8 = undefined;
var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
const allocator = fba.allocator();

pub fn main(boot_drive: std.os.uefi.Handle) !noreturn {
    var bip: ?*std.os.uefi.protocols.BlockIoProtocol = null;
    std.os.uefi.system_table.boot_services.?.handleProtocol(
        boot_drive,
        &std.os.uefi.protocols.BlockIoProtocol.guid,
        @ptrCast(&bip),
    ).err() catch |err| std.debug.panic("failed to handle block IO protocol: {s}", .{@errorName(err)});

    const boot_device = BlockDevice.init(allocator, bip.?) catch |err| std.debug.panic(
        "failed to initialize block device: {s}",
        .{@errorName(err)},
    );

    try gop.init();

    stage3.main(
        boot_device,
        allocator,
        requestPageFrame,
        mapPageToAnyVirt,
        mapRangeToAnyVirt,
        gop.queryNextVideoMode,
        gop.setVideoMode,
        finalizeBoot,
        acpi.findSdp() orelse @panic("no RSDP/XSDP"),
    );
}
