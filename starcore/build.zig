const std = @import("std");

const x86_64_freestanding_target = blk: {
    var sub_features = std.Target.Cpu.Feature.Set.empty;
    var add_features = std.Target.Cpu.Feature.Set.empty;
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.mmx));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.sse));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.sse2));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.avx));
    sub_features.addFeature(@intFromEnum(std.Target.x86.Feature.avx2));
    add_features.addFeature(@intFromEnum(std.Target.x86.Feature.soft_float));
    break :blk std.zig.CrossTarget{
        .cpu_arch = std.Target.Cpu.Arch.x86_64,
        .os_tag = std.Target.Os.Tag.freestanding,
        .abi = std.Target.Abi.none,
        .cpu_features_sub = sub_features,
        .cpu_features_add = add_features,
    };
};

pub fn build(b: *std.Build) void {
    const starcore_elf = b.addExecutable(.{
        .name = "starcore",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = x86_64_freestanding_target,
        .optimize = b.standardOptimizeOption(.{}),
    });

    starcore_elf.red_zone = false;
    // starcore_elf.bundle_compiler_rt = false;
    starcore_elf.dwarf_format = .@"64";
    starcore_elf.code_model = .large;
    starcore_elf.addAssemblyFile(.{ .path = "src/entry.S" });
    starcore_elf.addAssemblyFile(.{ .path = "src/hardware/gdt.S" });
    starcore_elf.setLinkerScript(.{ .path = "src/linker.ld" });
    starcore_elf.addAnonymousModule("helios", .{
        .source_file = .{ .path = "../helios/helios.zig" },
    });
    b.getInstallStep().dependOn(&b.addInstallArtifact(starcore_elf, .{ .dest_dir = .{ .override = .{ .custom = "boot" } } }).step);
}
