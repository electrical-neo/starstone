const std = @import("std");
const vmm = @import("memory/vmm.zig");

pub const SdtHeader = extern struct {
    signature: [4]u8 align(1),
    length: u32 align(1),
    revision: u8 align(1),
    checksum: u8 align(1),
    oem_id: [6]u8 align(1),
    oem_table_id: [8]u8 align(1),
    oem_revision: u32 align(1),
    creator_id: u32 align(1),
    creator_revision: u32 align(1),
    data: void align(1),

    pub fn doChecksum(self: *const SdtHeader) bool {
        var sum: u8 = 0;

        for (0..self.length) |i| {
            sum +%= @as(*const u8, @ptrFromInt(@intFromPtr(self) + i)).*;
        }

        return sum == 0;
    }
};

pub const Rsdt = extern struct {
    sdt: SdtHeader align(1),
    entries: void align(1),

    pub fn findSdt(self: *align(1) const @This(), comptime Sdt: type) !*align(1) Sdt {
        return for (0..(self.sdt.length - @sizeOf(SdtHeader)) / 4) |i| {
            const sdt = try mapSdt(Sdt, @as([*]align(1) const u32, @ptrCast(&self.entries))[i]);
            if (std.mem.eql(u8, &sdt.sdt.signature, Sdt.signature)) {
                if (!sdt.sdt.doChecksum())
                    break error.InvalidChecksum;
                break @ptrCast(sdt);
            }
        } else error.NotFound;
    }
};

pub const Xsdt = extern struct {
    sdt: SdtHeader align(1),
    entries: void align(1),

    pub fn findSdt(self: *align(1) const @This(), comptime Sdt: type) !*align(1) Sdt {
        return for (0..(self.sdt.length - @sizeOf(SdtHeader)) / 8) |i| {
            const sdt = try mapSdt(Sdt, @as([*]align(1) const u64, @ptrCast(&self.entries))[i]);
            if (std.mem.eql(u8, &sdt.sdt.signature, Sdt.signature)) {
                if (!sdt.sdt.doChecksum())
                    break error.InvalidChecksum;
                break @ptrCast(sdt);
            }
        } else error.NotFound;
    }
};

pub const Madt = extern struct {
    const signature = "APIC";

    pub const Entry = extern struct {
        const Type = enum(u8) {
            processor_local_apic = 0,
            _,
        };

        type: Type align(1),
        length: u8 align(1),
        data: extern union {
            processor_local_apic: extern struct {
                acpi_processor_id: u8 align(1),
                apic_id: u8 align(1),
                flags: u32 align(1),
            } align(1),
        },

        pub const Iterator = struct {
            madt: *const Madt,
            offset: usize = @sizeOf(Madt),

            pub fn next(self: *@This()) ?*const Entry {
                if (self.offset >= self.madt.sdt.length)
                    return null;

                const entry: *const Entry = @ptrFromInt(@intFromPtr(self.madt) + self.offset);
                defer self.offset += entry.length;
                return entry;
            }
        };
    };

    sdt: SdtHeader align(1),
    local_apic: u32 align(1),
    flags: u32 align(1),
    entries: void align(1),
};

pub fn mapSdt(comptime Sdt: type, phys: u64) !*align(1) Sdt {
    var sdt = try vmm.mapItem(SdtHeader, phys, .{});
    sdt = @ptrCast(try vmm.remapArea(sdt, sdt.length));

    if (!sdt.doChecksum())
        return error.InvalidChecksum;

    return @ptrCast(sdt);
}
