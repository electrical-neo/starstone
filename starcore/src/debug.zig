const std = @import("std");
const helios = @import("helios");

var dwarf_info: ?std.dwarf.DwarfInfo = null;
var _allocator: ?std.mem.Allocator = null;

pub fn init(allocator: std.mem.Allocator) !void {
    var sections: std.dwarf.DwarfInfo.SectionArray = std.dwarf.DwarfInfo.null_section_array;

    inline for (@typeInfo(helios.boot.DwarfSection).Enum.fields, 0..) |sect, i| {
        if (helios.boot.boot_info.dwarf[i].base != 0) {
            sections[@intFromEnum(@field(std.dwarf.DwarfSection, sect.name))] = .{
                .data = @as([*]const u8, @ptrFromInt(helios.boot.boot_info.dwarf[i].base))[0..helios.boot.boot_info.dwarf[i].size],
                .owned = true,
            };
        }
    }

    dwarf_info = std.dwarf.DwarfInfo{
        .endian = .Little,
        .sections = sections,
        .is_macho = false,
    };
    _allocator = allocator;
    try std.dwarf.openDwarfDebugInfo(&dwarf_info.?, allocator);
}

pub fn printAddressInfo(writer: anytype, addr: usize) !void {
    writer.print("0x{x}\n", .{addr}) catch {};

    if (dwarf_info == null or _allocator == null)
        return error.DebugNotInitialized;

    const comp_unit = try dwarf_info.?.findCompileUnit(addr);
    const line = try dwarf_info.?.getLineNumberInfo(_allocator.?, comp_unit.*, addr);
    defer line.deinit(_allocator.?);

    writer.print("    {s}:{}:{}\n", .{ line.file_name, line.line, line.column }) catch {};
}
