const helios = @import("helios");
const vmm = @import("../memory/vmm.zig");
const timer = @import("timer.zig");
const idt = @import("idt.zig");

var regs: ?[*]align(1) volatile u32 = null;

pub fn init() !void {
    asm volatile ("cli");
    if (asm volatile ("cpuid"
        : [rdx] "={rdx}" (-> u64),
        : [code] "{rax}" (1),
        : "rbx", "rcx"
    ) & (1 << 9) == 0) {
        return error.NoAPIC;
    }

    const base = get_base();
    set_base(base);
    regs = @ptrCast(try vmm.mapArea(base, 0x3f0, .{ .cache_disable = true }));
    helios.hardware.qemu.debug_con.print("APIC at 0x{x}\n", .{base}) catch {};

    regs.?[0xe0 / 4] = 0xffffffff;
    regs.?[0xd0 / 4] = (regs.?[0xd0] & 0xffffff) | 1;
    regs.?[0x320 / 4] = 0x10000;
    regs.?[0x340 / 4] = 4 << 8;
    regs.?[0x350 / 4] = 0x10000;
    regs.?[0x360 / 4] = 0x10000;
    regs.?[0x80 / 4] = 0;
    regs.?[0xf0 / 4] = 0x1ff;

    // calibrate the timer
    regs.?[0x3e0 / 4] = 3; // divisor
    regs.?[0x320 / 4] = 0x20; // one shot, irq 0x20

    helios.hardware.port.outb(0x61, (helios.hardware.port.inb(0x61) & 0xfd) | 1);
    helios.hardware.port.outb(0x43, 0xb2);
    helios.hardware.port.outb(0x42, 0x9b);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0x42, 0x2e);
    const x = helios.hardware.port.inb(0x61) & 0xfe;
    helios.hardware.port.outb(0x61, x);
    helios.hardware.port.outb(0x61, x | 1);

    asm volatile (
        \\ mov $0x61, %dx
        \\ movl $3, 0x3e0(%[apic])
        \\ movl $0x20, 0x320(%[apic])
        \\ movl $0xffffffff, 0x380(%[apic])
        \\ 1:
        \\ in %dx, %al
        \\ and $0x20, %al
        \\ jz 1b
        \\ movl $0x10000, 0x320(%[apic])
        :
        : [apic] "{rbx}" (regs.?),
        : "dx", "al"
    );

    const freq = (0xffffffff - regs.?[0x390 / 4] + 1) * 16 * 100;
    helios.hardware.qemu.debug_con.print("CPU bus frequency: {} MHz\n", .{freq / 1000 / 1000}) catch {};

    timer.ms_every_tick = 1;
    regs.?[0x3e0 / 4] = 3; // divisor
    regs.?[0x320 / 4] = 0x20;
    regs.?[0x380 / 4] = freq / 16 / 1000; // initial count
    asm volatile ("sti");
}

pub inline fn eoi() void {
    @setRuntimeSafety(false);
    regs.?[0xb0 / 4] = 0;
}

fn set_base(base: usize) void {
    asm volatile ("wrmsr"
        :
        : [low] "{eax}" ((base & 0xfffff000) | 0x800),
          [high] "{edx}" (base >> 32),
          [msr] "{ecx}" (0x1b),
    );
}

fn get_base() usize {
    var low: u32 = undefined;
    var high: u32 = undefined;

    asm volatile ("rdmsr"
        : [low] "={eax}" (low),
          [high] "={edx}" (high),
        : [msr] "{ecx}" (0x1b),
    );

    return (low & 0xfffff000) | (@as(usize, high) << 32);
}

pub export fn timerHandler(frame: *idt.InterruptFrame) callconv(.C) void {
    timer.timerHandler(frame);
    regs.?[0x380 / 4] = regs.?[0x380 / 4];
}
