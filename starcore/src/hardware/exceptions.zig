const std = @import("std");
const idt = @import("idt.zig");
const vmm = @import("../memory/vmm.zig");

pub const Exception = enum(u8) {
    division_error = 0x00,
    debug = 0x01,
    nmi = 0x02,
    breakpoint = 0x03,
    overflow = 0x04,
    bound_range_exceeded = 0x05,
    invalid_opcode = 0x06,
    device_not_available = 0x07,
    double_fault = 0x08,
    invalid_tss = 0x0a,
    segment_not_present = 0x0b,
    stack_segment_fault = 0x0c,
    general_protection_fault = 0x0d,
    page_fault = 0xe,
    x87_fp_exception = 0x10,
    alignment_check = 0x11,
    machine_check = 0x12,
    simd_fp_exception = 0x13,
    virtualization_exception = 0x14,
    control_protection_exception = 0x15,
    hypervisor_injection_exception = 0x1c,
    vmm_communication_exception = 0x1d,
    security_exception = 0x1e,

    pub fn hasErrorCode(self: @This()) bool {
        return switch (self) {
            .double_fault => true,
            .invalid_tss => true,
            .segment_not_present => true,
            .stack_segment_fault => true,
            .general_protection_fault => true,
            .page_fault => true,
            .alignment_check => true,
            .control_protection_exception => true,
            .vmm_communication_exception => true,
            .security_exception => true,
            else => false,
        };
    }
};

pub export fn pageFaultHandler(frame: *idt.InterruptFrame) callconv(.C) void {
    _ = frame;

    const virt = asm volatile ("mov %%cr2, %[result]"
        : [result] "=r" (-> usize),
    );

    vmm.access(virt) catch |err| {
        std.debug.panic("cannot access memory at 0x{x}: {s}", .{ virt, @errorName(err) });
    };
}

pub fn commonExceptionHandler(comptime e: Exception) fn (*idt.InterruptFrame) callconv(.C) void {
    return struct {
        comptime {
            @export(handler, .{ .name = "exception_" ++ @tagName(e) });
        }

        fn handler(frame: *idt.InterruptFrame) callconv(.C) noreturn {
            std.debug.panic("exception {s} (code: {?d})", .{
                @tagName(e),
                if (e.hasErrorCode()) frame.error_code else null,
            });
        }
    }.handler;
}
