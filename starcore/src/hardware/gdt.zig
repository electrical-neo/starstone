const std = @import("std");

const GdtEntry = packed struct {
    limit_low: u16 = 0,
    base_low: u24 = 0,
    accessed: bool = false,
    read_write: bool,
    direction_conforming: bool,
    executable: bool,
    not_system: bool,
    dpl: u2,
    present: bool = true,
    limit_high: u4 = 0,
    reserved: bool = false,
    long_mode_code: bool,
    size: bool,
    granularity: bool = true,
    base_high: u8 = 0,
};

const Gdtr = extern struct {
    size: u16,
    offset: usize align(1),
};

var gdt: extern struct {
    null: GdtEntry = @bitCast(@as(u64, 0)),
    kernel_code: GdtEntry = .{
        .read_write = true,
        .direction_conforming = false,
        .executable = true,
        .not_system = true,
        .dpl = 0,
        .long_mode_code = true,
        .size = false,
    },
    kernel_data: GdtEntry = .{
        .read_write = true,
        .direction_conforming = false,
        .executable = false,
        .not_system = true,
        .dpl = 0,
        .long_mode_code = false,
        .size = true,
    },
} = .{};

extern fn gdt_init(gdtr: usize) callconv(.C) void;
pub fn init() void {
    const gdtr = Gdtr{
        .size = @sizeOf(@TypeOf(gdt)) - 1,
        .offset = @intFromPtr(&gdt),
    };

    gdt_init(@intFromPtr(&gdtr));
}
