const exceptions = @import("exceptions.zig");
const timer = @import("timer.zig");
const pic = @import("pic.zig");
const apic = @import("apic.zig");

pub const SavedRegisters = packed struct {
    rax: u64,
    rbx: u64,
    rcx: u64,
    rdx: u64,
    rsi: u64,
    rdi: u64,
    rbp: u64,
    r8: u64,
    r9: u64,
    r10: u64,
    r11: u64,
    r12: u64,
    r13: u64,
    r14: u64,
    r15: u64,
};

pub const InterruptFrame = packed struct {
    regs: SavedRegisters,
    error_code: u64,
    ip: u64,
    cs: u64,
    flags: u64,
    sp: u64,
    ss: u64,
};

pub const IdtEntry = packed struct {
    const Type = enum(u4) {
        none = 0,
        interrupt = 0xe,
        trap = 0xf,
    };

    offset_low: u16,
    segment: u16,
    ist: u3 = 0,
    reserved0: u5 = 0,
    type: Type,
    zero: u1 = 0,
    dpl: u2,
    p: bool = true,
    offset_high: u48,
    reserved1: u32 = 0,

    const ignore_int: @This() = @bitCast(@as(u128, 0));

    pub fn init(offset: u64, segment: u16, typ: Type, dpl: u2) @This() {
        return .{
            .offset_low = @intCast(offset & 0xffff),
            .segment = segment,
            .type = typ,
            .dpl = dpl,
            .offset_high = @intCast(offset >> 16),
        };
    }
};

const Idtr = extern struct {
    size: u16,
    offset: usize align(1),
};

fn ignoreInt() callconv(.Naked) void {
    asm volatile ("iretq");
}

pub fn interruptHandler(
    comptime h: fn (*InterruptFrame) callconv(.C) void,
    comptime has_error_code: bool,
    comptime eoi: ?fn () callconv(.Inline) void,
) fn () callconv(.Naked) void {
    return struct {
        fn f() callconv(.Naked) void {
            if (!has_error_code) {
                // Pretend there is an error code
                asm volatile ("sub $8, %rsp");
            }

            asm volatile (
                \\
                // Save registers
                \\ push %r15
                \\ push %r14
                \\ push %r13
                \\ push %r12
                \\ push %r11
                \\ push %r10
                \\ push %r9
                \\ push %r8
                \\ push %rbp
                \\ push %rdi
                \\ push %rsi
                \\ push %rdx
                \\ push %rcx
                \\ push %rbx
                \\ push %rax

                // Save the unaligned stack (the interrupt frame)
                \\ mov %rsp, %rbx

                // Align the stack
                \\ and $~0xf, %rsp

                // Push the return address like call would
                // All of this is done to give a good stack trace
                \\ push (16 * 8)(%rbx)
                // \\ push (15 * 8)(%rbx)

                // Create a stack frame, call the handler and pop the frame
                \\ push %rbp
                \\ mov %rsp, %rbp
                \\ mov %rbx, %rdi
                \\ movabs %[handler], %rax
                \\ call *%rax
                \\ pop %rbp

                // Pop the return address like ret would
                \\ add $8, %rsp

                // Restore the stack before aligning
                \\ mov %rbx, %rsp
                :
                : [handler] "p" (h),
            );

            if (eoi) |i|
                i();

            asm volatile (
            // Restore registers
                \\ pop %rax
                \\ pop %rbx
                \\ pop %rcx
                \\ pop %rdx
                \\ pop %rsi
                \\ pop %rdi
                \\ pop %rbp
                \\ pop %r8
                \\ pop %r9
                \\ pop %r10
                \\ pop %r11
                \\ pop %r12
                \\ pop %r13
                \\ pop %r14
                \\ pop %r15

                // Pop the error code
                \\ add $8, %rsp
                \\iretq
            );
        }
    }.f;
}

pub var idt: [256]IdtEntry align(4096) = .{IdtEntry.ignore_int} ** 256;

pub fn init() !void {
    idt[@intFromEnum(exceptions.Exception.page_fault)] = IdtEntry.init(
        @intFromPtr(&interruptHandler(exceptions.pageFaultHandler, true, null)),
        0x8,
        .trap,
        0,
    );

    inline for (@typeInfo(exceptions.Exception).Enum.fields) |e| {
        if (!idt[e.value].p) {
            idt[e.value] = IdtEntry.init(
                @intFromPtr(&interruptHandler(
                    exceptions.commonExceptionHandler(@enumFromInt(e.value)),
                    @as(exceptions.Exception, @enumFromInt(e.value)).hasErrorCode(),
                    null,
                )),
                0x8,
                .trap,
                0,
            );
        }
    }

    idt[0x20] = IdtEntry.init(
        @intFromPtr(&interruptHandler(apic.timerHandler, false, apic.eoi)),
        0x8,
        .interrupt,
        0,
    );

    idt[0x27] = IdtEntry.init(@intFromPtr(&ignoreInt), 0x8, .interrupt, 0);
    idt[0x2f] = IdtEntry.init(@intFromPtr(&ignoreInt), 0x8, .interrupt, 0);
    idt[0xff] = IdtEntry.init(@intFromPtr(&ignoreInt), 0x8, .interrupt, 0);

    const idtr = Idtr{
        .size = @sizeOf(@TypeOf(idt)) - 1,
        .offset = @intFromPtr(&idt),
    };

    pic.disable();

    asm volatile ("lidt (%[idtr])"
        :
        : [idtr] "r" (&idtr),
    );

    asm volatile ("sti");
}
