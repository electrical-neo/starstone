const helios = @import("helios");

pub fn disable() void {
    helios.hardware.port.outb(0x20, 0x11);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0xa0, 0x11);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0x21, 0x20);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0xa1, 0x28);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0x21, 0x2);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0xa1, 0x4);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0x21, 0x1);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0xa1, 0x1);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0x21, 0xff);
    helios.hardware.port.wait();
    helios.hardware.port.outb(0xa1, 0xff);
    helios.hardware.port.wait();
}
