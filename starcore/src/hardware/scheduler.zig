const std = @import("std");
const idt = @import("idt.zig");

const Process = struct {
    regs: idt.SavedRegisters,
    ip: u64,
    cs: u64,
    flags: u64,
    sp: u64,
    ss: u64,

    pub fn restore(self: *const Process, frame: *idt.InterruptFrame) void {
        frame.regs = self.regs;
        frame.ip = self.ip;
        frame.cs = self.cs;
        frame.flags = self.flags | 0x200; // make sure not to disable interrupts
        frame.sp = self.sp;
        frame.ss = self.ss;
    }

    pub fn save(self: *Process, frame: *const idt.InterruptFrame) void {
        self.regs = frame.regs;
        self.ip = frame.ip;
        self.cs = frame.cs;
        self.flags = frame.flags;
        self.sp = frame.sp;
        self.ss = frame.ss;
    }
};

var current_process: usize = 0;
var processes: std.ArrayList(Process) = undefined;

pub fn init(allocator: std.mem.Allocator) !void {
    processes = std.ArrayList(Process).init(allocator);
    try processes.append(undefined); // this process will be overwritten
}

pub fn spawnProcess(ip: usize, sp: usize, flags: usize) !void {
    var regs: idt.SavedRegisters = undefined;
    regs.rbp = sp;
    try processes.append(.{
        .regs = regs,
        .ip = ip,
        .cs = 0x8,
        .flags = flags,
        .sp = sp,
        .ss = 0x10,
    });
}

pub fn contextSwitch(frame: *idt.InterruptFrame) void {
    processes.items[current_process].save(frame);
    current_process += 1;
    if (current_process == processes.items.len)
        current_process = 0;
    processes.items[current_process].restore(frame);
}
