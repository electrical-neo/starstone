const idt = @import("idt.zig");
const scheduler = @import("scheduler.zig");

pub var ms_since_boot: u64 = 0;
pub var ms_every_tick: u64 = 0;

pub fn timerHandler(frame: *idt.InterruptFrame) void {
    ms_since_boot += ms_every_tick;
    // _ = frame;
    scheduler.contextSwitch(frame);
}

pub fn sleep(ms: u64) void {
    const resume_time = ms_since_boot + ms;
    while (ms_since_boot < resume_time) {
        asm volatile ("hlt");
    }
}
