const std = @import("std");
const helios = @import("helios");
const debug = @import("debug.zig");
const gdt = @import("hardware/gdt.zig");
const idt = @import("hardware/idt.zig");
const scheduler = @import("hardware/scheduler.zig");
const pmm = @import("memory/pmm.zig");
const vmm = @import("memory/vmm.zig");
const acpi = @import("acpi.zig");
const apic = @import("hardware/apic.zig");
const timer = @import("hardware/timer.zig");

var terminal: ?helios.Terminal = null;

var panicked = false;
pub fn panic(message: []const u8, _: ?*std.builtin.StackTrace, _: ?usize) noreturn {
    @setCold(true);
    asm volatile ("cli");

    if (terminal) |*t| {
        const writer = t.writer();
        writer.print("\n!!!!! KERNEL PANIC !!!!!\n", .{}) catch {};
        writer.print("Info: {s}\n", .{message}) catch {};

        if (panicked) {
            writer.print("Recursive panic detected. Stopping", .{}) catch {};
            while (true) asm volatile ("hlt");
        }
        panicked = true;

        writer.print("Stack trace:\n", .{}) catch {};
        var it = std.debug.StackIterator.init(@returnAddress(), @frameAddress());
        while (it.next()) |addr| {
            debug.printAddressInfo(writer, addr) catch |err| {
                writer.print("Failed to get address info: {s}\n", .{@errorName(err)}) catch {};
            };
        }
    }

    while (true) asm volatile ("hlt");
}

extern const kernel_start: opaque {};
extern const kernel_end: opaque {};

var fba_buf: [8 * 1024 * 1024]u8 = undefined;

var fb: helios.Framebuffer = undefined;

fn taskB() callconv(.C) noreturn {
    var term = helios.Terminal{
        .fb = fb.rectangle(fb.width / 2 + 1, 0, fb.width / 2 - 1, fb.height / 2) catch while (true) {
            asm volatile ("hlt");
        },
        .font = helios.boot.font,
    };

    term.clear();
    const writer = term.writer();

    while (true) {
        writer.print("Test 2\n", .{}) catch {};
    }
}

var stack_b: [8192]u8 align(16) = undefined;

fn taskC() callconv(.C) noreturn {
    var term = helios.Terminal{
        .fb = fb.rectangle(fb.width / 2 + 1, fb.height / 2 + 1, fb.width / 2 - 1, fb.height / 2 - 1) catch while (true) {
            asm volatile ("hlt");
        },
        .font = helios.boot.font,
    };

    term.clear();
    const writer = term.writer();

    while (true) {
        writer.print("Test 3\n", .{}) catch {};
    }
}

var stack_c: [8192]u8 align(16) = undefined;

export fn main() callconv(.C) void {
    gdt.init();

    var fba = std.heap.FixedBufferAllocator.init(&fba_buf);
    const allocator = fba.allocator();

    fb = .{
        .base = helios.boot.boot_info.framebuffer.base,
        .width = helios.boot.boot_info.framebuffer.width,
        .height = helios.boot.boot_info.framebuffer.height,
        .pitch = helios.boot.boot_info.framebuffer.pitch,
        .pixel_size = helios.boot.boot_info.framebuffer.pixel_size,
    };

    terminal = .{
        .fb = fb.rectangle(0, 0, fb.width / 2, fb.height) catch |err| @panic(@errorName(err)),
        .font = helios.boot.font,
    };

    terminal.?.clear();
    const writer = terminal.?.writer();
    writer.print("Starstone ALPHA\n", .{}) catch {};

    debug.init(allocator) catch |err| {
        writer.print("[ WARN ] Stack trace will not be available: {s}\n", .{@errorName(err)}) catch {};
    };

    scheduler.init(allocator) catch |err| {
        std.debug.panic("cannot initialize the scheduler: {s}", .{@errorName(err)});
    };

    // scheduler.spawnProcess(
    //     @intFromPtr(&taskB),
    //     @intFromPtr(&stack_b) + stack_b.len,
    //     0x246,
    // ) catch |err| {
    //     std.debug.panic("cannot spawn a new process: {s}\n", .{@errorName(err)});
    // };
    //
    // scheduler.spawnProcess(
    //     @intFromPtr(&taskC),
    //     @intFromPtr(&stack_c) + stack_c.len,
    //     0x246,
    // ) catch |err| {
    //     std.debug.panic("cannot spawn a new process: {s}\n", .{@errorName(err)});
    // };

    idt.init() catch |err| @panic(@errorName(err));

    pmm.init(allocator) catch |err| {
        std.debug.panic("cannot initialize PMM: {s}", .{@errorName(err)});
    };

    const rsdt_addr = helios.boot.boot_info.rsdt;
    const xsdt_addr = helios.boot.boot_info.xsdt;
    vmm.init() catch |err| {
        std.debug.panic("cannot initialize VMM: {s}", .{@errorName(err)});
    };

    writer.print("Kernel start: 0x{x:0>16}\nKernel   end: 0x{x:0>16}\n", .{
        @intFromPtr(&kernel_start),
        @intFromPtr(&kernel_end),
    }) catch {};

    vmm.markAreaUsed(@intFromPtr(&kernel_start), (@intFromPtr(&kernel_end) - @intFromPtr(&kernel_start))) catch |err| {
        std.debug.panic("cannot mark memory as used: {s}", .{@errorName(err)});
    };

    vmm.markAreaUsed(helios.boot.boot_info.framebuffer.base & ~@as(usize, 0xfff), helios.boot.boot_info.framebuffer.pitch * helios.boot.boot_info.framebuffer.height) catch |err| {
        std.debug.panic("cannot mark memory as used: {s}", .{@errorName(err)});
    };

    vmm.markAreaUsed(@intFromPtr(helios.boot.font), @sizeOf(@TypeOf(helios.boot.font.*))) catch |err| {
        std.debug.panic("cannot mark memory as used: {s}", .{@errorName(err)});
    };

    apic.init() catch |err| @panic(@errorName(err));

    const madt = if (rsdt_addr != 0) blk: {
        const rsdt = acpi.mapSdt(acpi.Rsdt, rsdt_addr) catch |err| @panic(@errorName(err));
        break :blk rsdt.findSdt(acpi.Madt) catch |err| @panic(@errorName(err));
    } else blk: {
        const xsdt = acpi.mapSdt(acpi.Xsdt, xsdt_addr) catch |err| @panic(@errorName(err));
        break :blk xsdt.findSdt(acpi.Madt) catch |err| @panic(@errorName(err));
    };

    var madt_it = acpi.Madt.Entry.Iterator{ .madt = madt };

    var smp_cores: u64 = 0;
    while (madt_it.next()) |e| {
        if (e.type == .processor_local_apic) {
            if (e.data.processor_local_apic.flags & 1 != 0)
                smp_cores += 1;
        }
    }

    writer.print("Detected {} usable CPU cores\n", .{smp_cores}) catch {};

    var seconds: usize = 0;
    while (true) {
        timer.sleep(1000);
        writer.print("[{}.{:0>3}] Tick\n", .{ timer.ms_since_boot / 1000, timer.ms_since_boot % 1000 }) catch {};
        seconds += 1;
    }
}
