const std = @import("std");
const helios = @import("helios");

const PageBitmap = struct {
    base: u64,
    bitmap: std.DynamicBitSet,

    fn init(allocator: std.mem.Allocator, base: u64, length: u64) !?@This() {
        const end = base + length;
        const page_base = (base + 0xfff) & ~@as(u64, 0xfff);
        const page_end = end & ~@as(u64, 0xfff);
        const page_count = (page_end - page_base) / 0x1000;
        if (page_count == 0)
            return null;

        return .{
            .base = page_base,
            .bitmap = try std.DynamicBitSet.initFull(allocator, page_count),
        };
    }
};

var bitmaps: ?[]PageBitmap = null;

pub fn init(allocator: std.mem.Allocator) !void {
    var bitmaps_arrlist = std.ArrayList(PageBitmap).init(allocator);

    var base: u64 = 0;
    for (helios.boot.mmap[0..helios.boot.boot_info.mmap_entries]) |e| {
        defer base += e.length;

        if (e.type == .free) {
            try bitmaps_arrlist.append(try PageBitmap.init(allocator, base, e.length) orelse continue);
        }
    }

    bitmaps = try bitmaps_arrlist.toOwnedSlice();
}

pub fn print(stdout: anytype) !void {
    if (bitmaps) |b| {
        var mem: u64 = 0;
        for (b) |bitmap| {
            mem += bitmap.bitmap.capacity();
            try stdout.print("0x{x:0>16} | {} KB\n", .{ bitmap.base, bitmap.bitmap.capacity() * 4 });
        }

        try stdout.print("Usable memory: {} KB\n", .{mem * 4});
    } else {
        return error.PmmNotInitialized;
    }
}

pub fn requestPageFrame() !u64 {
    if (bitmaps) |b| {
        for (b) |*bitmap| {
            if (bitmap.bitmap.findFirstSet()) |p| {
                bitmap.bitmap.unset(p);
                return bitmap.base + p * 4096;
            }
        }
        return error.OutOfMemory;
    } else {
        return error.PmmNotInitialized;
    }
}
