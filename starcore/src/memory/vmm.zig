const std = @import("std");
const helios = @import("helios");
const pmm = @import("pmm.zig");

// TODO: freeing

const MemoryMapping = struct {
    const Type = enum {
        allocate,
        map,
        used,
        unused,
    };

    pages: u52,
    next: ?*MemoryMapping = null,
    prev: ?*MemoryMapping = null,

    type: union(Type) {
        allocate: struct {
            fn access(self: *@This(), virt_page: u52, page: u52) !void {
                _ = self;
                const pf = try pmm.requestPageFrame();
                _ = try helios.memory.vmm.mapPage(pmm, pf, @as(usize, virt_page + page) * 0x1000, .{});
            }
        },
        map: struct {
            phys: u52,
            phys_offset: usize,
            flags: helios.memory.vmm.PageFlags,
            fn access(self: *@This(), virt_page: u52, page: u52) !void {
                _ = try helios.memory.vmm.mapPage(pmm, @as(usize, self.phys + page) * 0x1000, @as(usize, virt_page + page) * 0x1000, self.flags);
            }
        },
        used: void,
        unused: void,
    },

    // Called if accessing this mapping triggers a PF
    fn access(self: *@This(), virt_page: u52, page: u52) !void {
        return switch (self.type) {
            .allocate => |*x| x.access(virt_page, page),
            .map => |*x| x.access(virt_page, page),
            // this region should be mapped, so the reason for a PF might be an access violation
            .used => error.AccessViolation,
            .unused => error.InvalidAddress,
        };
    }

    fn split(self: *@This(), len: u52) !*@This() {
        if (self.type != .unused)
            return error.InUse;

        if (self.pages == len)
            return self;

        const new = try fba.allocator().create(@This());
        new.* = .{
            .type = .unused,
            .pages = self.pages - len,
            .next = self.next,
            .prev = self,
        };

        if (self.next) |n| {
            n.prev = new;
        }

        self.next = new;
        self.pages = len;

        return new;
    }

    fn setUsed(self: *@This()) void {
        self.type = .used;
        if (self.next) |next| {
            if (next.type == .used) {
                self.pages += next.pages;
                self.next = next.next;
                if (next.next) |next_next|
                    next_next.prev = self;
                fba.allocator().destroy(next);
            }
        }

        if (self.prev) |prev| {
            if (prev.type == .used) {
                self.pages += prev.pages;
                self.prev = prev.prev;
                if (prev.prev) |prev_prev|
                    prev_prev.next = self;
                fba.allocator().destroy(prev);
            }
        }
    }

    fn expand(self: *@This(), add_pages: u52) !void {
        if (self.next == null)
            return error.CannotExpand;

        if (self.next.?.type != .unused)
            return error.CannotExpand;

        if (self.next.?.pages < add_pages)
            return error.CannotExpand;

        self.pages += add_pages;
        self.next.?.pages -= add_pages;
    }
};

var mappings: ?*MemoryMapping = null;
var fba: std.heap.FixedBufferAllocator = undefined;

pub fn init() !void {
    if (mappings != null)
        return error.AlreadyInitialized;

    const pf = try pmm.requestPageFrame();
    _ = try helios.memory.vmm.mapPage(pmm, pf, 0x1000, .{});
    fba = std.heap.FixedBufferAllocator.init(@as([*]u8, @ptrFromInt(0x1000))[0..0x1000]);

    mappings = try fba.allocator().create(MemoryMapping);
    mappings.?.* = .{
        .pages = 512 * 1024 * 1024 * 1024 / 4096 - 1,
        .type = .{ .allocate = .{} },
    };

    fba.buffer = @as([*]u8, @ptrFromInt(0x1000))[0 .. 512 * 1024 * 1024 * 1024 - 0x1000];

    mappings.?.next = try fba.allocator().create(MemoryMapping);
    mappings.?.next.?.* = .{
        .pages = 0xfffffffffffff - 512 * 1024 * 1024 * 1024 / 4096,
        .type = .unused,
        .prev = mappings.?,
    };
}

pub fn printMappings(writer: anytype) !void {
    if (mappings == null)
        return error.VmmNotInitialized;

    var ma = mappings;
    var virt_page: u52 = 1;
    while (ma) |m| : (ma = m.next) {
        try writer.print("0x{x:0>16} | 0x{x:0>16} | {s}\n", .{ @as(usize, virt_page) * 0x1000, @as(usize, m.pages + virt_page) * 0x1000, @tagName(m.type) });
        virt_page += m.pages;
    }
}

pub fn access(virt: usize) !void {
    if (mappings == null)
        return error.VmmNotInitialized;

    const page = virt & ~@as(usize, 0xfff);

    var ma = mappings;
    var virt_page: u52 = 1;
    while (ma) |m| : (ma = m.next) {
        if (page / 4096 >= virt_page and page / 4096 < virt_page + m.pages) {
            return m.access(virt_page, @intCast(page / 4096 - virt_page));
        }
        virt_page += m.pages;
    }

    return error.InvalidAddress;
}

pub fn markAreaUsed(start: usize, size: usize) !void {
    const page: u52 = @intCast(start / 4096);
    const pages: u52 = @intCast((start + size + 0xfff) / 4096 - page);

    var ma = mappings;
    var virt_page: u52 = 1;

    while (ma) |m| {
        const chunk_pages = m.pages;
        defer virt_page += chunk_pages;

        if (virt_page > page + pages)
            return;

        if (m.type != .unused) {
            ma = m.next;
            continue;
        }

        if (page > virt_page and page < virt_page + m.pages and page + pages > virt_page + m.pages) {
            // end overlaps
            const new = try m.split(page - virt_page);
            new.setUsed();
            ma = new.next;
        } else if (page <= virt_page and page + pages > virt_page and page + pages < virt_page + m.pages) {
            // beginning overlaps
            _ = try m.split(page + pages - virt_page);
            m.setUsed();
            return;
        } else if (page > virt_page and page + pages < virt_page + m.pages) {
            // middle overlaps
            const middle = try m.split(page - virt_page);
            _ = try middle.split(pages);
            middle.setUsed();
            return;
        } else if (page <= virt_page and page + pages >= virt_page + m.pages) {
            // whole chunk overlaps
            m.setUsed();
            ma = m.next;
        } else {
            ma = m.next;
        }
    }
}

pub fn mapArea(phys: usize, size: usize, flags: helios.memory.vmm.PageFlags) !*anyopaque {
    if (mappings == null)
        return error.VmmNotInitialized;

    const page_phys: u52 = @intCast(phys / 4096);
    const pages: u52 = @intCast((phys + size + 0xfff) / 4096 - page_phys);

    var ma = mappings;
    var virt_page: u52 = 1;

    while (ma) |m| : (ma = m.next) {
        defer virt_page += m.pages;

        if (m.type != .unused) {
            ma = m.next;
            continue;
        }

        if (m.pages >= pages) {
            _ = try m.split(pages);
            m.type = .{ .map = .{
                .phys = page_phys,
                .phys_offset = phys,
                .flags = flags,
            } };

            return @ptrFromInt(@as(usize, virt_page) * 0x1000 + (phys & 0xfff));
        }
    }

    return error.OutOfMemory;
}

pub fn mapItem(comptime T: type, phys: usize, flags: helios.memory.vmm.PageFlags) !*align(1) T {
    if (mappings == null)
        return error.VmmNotInitialized;

    return @ptrCast(try mapArea(phys, @sizeOf(T), flags));
}

// WARNING: this function is pretty much untested. it's very likely it doesn't work.
pub fn remapArea(virt: anytype, size: usize) !*anyopaque {
    if (mappings == null)
        return error.VmmNotInitialized;

    var ma = mappings;
    var virt_page: u52 = 1;

    while (ma) |m| : (ma = m.next) {
        defer virt_page += m.pages;

        if (m.type != .map or virt_page != @intFromPtr(virt) / 4096) {
            ma = m.next;
            continue;
        }

        const page_phys = m.type.map.phys;
        const pages: u52 = @intCast((m.type.map.phys_offset + size + 0xfff) / 4096 - page_phys);

        if (pages <= m.pages)
            return @ptrCast(virt);

        m.expand(pages - m.pages) catch {
            // TODO: free this chunk
            return mapArea(m.type.map.phys_offset, size, m.type.map.flags);
        };

        return @ptrCast(virt);
    }

    return error.InvalidAddress;
}
